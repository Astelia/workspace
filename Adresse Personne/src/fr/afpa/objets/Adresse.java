package fr.afpa.objets;

public class Adresse {

	private int numero;
	private String libRue;
	private String codePostal;
	private String ville;
	
	public Adresse(int n, String lib, String cP, String v) {
		setNumero(n);
		setLibRue(lib);
		setCodePostal(cP);
		setVille(v);
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int n) {
		numero = n;
	}

	public String getLibRue() {
		return libRue;
	}

	public void setLibRue(String lib) {
		libRue = lib;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String cP) {
		this.codePostal = cP;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String v) {
		this.ville = v;
	}
	
}
