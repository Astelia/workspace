package fr.afpa.objets;

public class Personne {

	private String nom;
	private int age;
	private Adresse adresse;
	
	public Personne(String n, int a, Adresse add) {
		setNom(n);
		setAge(a);
		setAdresse(add);
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String n) {
		nom = n;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int a) {
		age = a;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse add) {
		adresse = add;
	}
	
}
