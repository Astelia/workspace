package fr.afpa.exercice6.objets;

public class Pays {

	private String nom;
	private String code;
	
	
	public Pays(String nom, String code) {
		super();
		this.nom = nom;
		this.code = code;
	}

	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		return this.nom.equals( ((Pays) obj).nom ) && this.code.equals( ((Pays) obj).code );
	}


	@Override
	public String toString() {
		return "Pays [nom=" + nom + ", code=" + code + "]";
	}	
	
}
