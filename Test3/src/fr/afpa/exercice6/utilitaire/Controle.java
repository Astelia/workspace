package fr.afpa.exercice6.utilitaire;

import java.time.LocalDate;

public class Controle {

	/**
	 * Fonction qui renvoie true si le numero de telephone passe en parametre est bien
	 * constitue de 10 chiffres et renvoie false sinon.
	 * @param tel
	 * @return
	 */
	public static boolean telephone(String tel) {
		return tel.matches("\\d{10}");
	}
	
	/**
	 * Fonction qui renvoie true si le code postal passe en parametre est bien constitue de 5 chiffres
	 * et renvoie false sinon.
	 * @param code
	 * @return
	 */
	public static boolean codePostal(String code) {
		return code.matches("\\d{5}");
	}
	
	/**
	 * Fonction qui renvoie true si le code pays passe en parametre est bien constitue de 2 caracteres
	 * alpahbetiques majuscules et renvoie false sinon.
	 * @param code
	 * @return
	 */
	public static boolean codePays(String code) {
		return code.matches("[A-Z]{2}");
	}
	
	/**
	 * Fonction qui renvoie true si l'email passe en parametre est bien sous ce format : 
	 * [3 carateres alphabetiques].[de 5 a 12 caracteres alphanumerique]@[de 4 a 8 caracteres alphanumerique].[de 2 a 3 caracteres alphabetique]
	 * et renvoie false sinon.
	 * @param mail
	 * @return
	 */
	public static boolean email(String mail) {
		return mail.matches("[a-zA-Z]{3}\\.[a-zA-Z0-9]{5,12}@[a-zA-Z0-9]{4,8}\\.[a-zA-Z]{2,3}");
	}
	
	/**
	 * Fonction qui renvoie true si la date passee en parametre est bien sous le format jj/mm/aaaa et est une date valide
	 * et renvoie false sinon.
	 * @param dateString
	 * @return
	 */
	public static boolean date(String dateString) {
		try {
			String[] dateTableau = dateString.split("/");
			LocalDate.of(Integer.parseInt(dateTableau[2]), Integer.parseInt(dateTableau[1]), Integer.parseInt(dateTableau[0]));
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * Fonction qui renvoie true si "nb" est un entier et false sinon.
	 * @param nb
	 * @return
	 */
	public static boolean entier(String nb) {
		try {
			Integer.parseInt(nb);
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * Fonction qui renvoie true si "nb" est un float et false sinon.
	 * @param nb
	 * @return
	 */
	public static boolean flottant(String nb) {
		try {
			Float.parseFloat(nb);
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}
	
}
