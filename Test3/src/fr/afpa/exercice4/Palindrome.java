package fr.afpa.exercice4;

public class Palindrome {

	public static void main(String[] args) {
		
		palindrome("LOL");
		System.out.println();
		palindrome("KAYAK");
		System.out.println();
		palindrome("ELLE");
		System.out.println();
		System.out.println();
		
		palindrome("TRUC");
		System.out.println();
		palindrome("BIDULE");
		System.out.println();
		palindrome("CHOUETTE");
		System.out.println();

	}
	
	/**
	 * Methode qui affiche VRAI si "mot" est un palindrome et FAUX sinon.
	 * @param mot
	 */
	public static void palindrome(String mot) {
		System.out.print(mot+" est un palindrome : ");
		String res = "VRAI";
		int i=0;
		while (i<mot.length()/2) {
			if (mot.charAt(i) != mot.charAt(mot.length()-1-i)) {
				res = "FAUX";
				break;
			}
			i++;
		}
		System.out.println(res);
	}

}
