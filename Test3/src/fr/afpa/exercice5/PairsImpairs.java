package fr.afpa.exercice5;

import java.util.ArrayList;
import java.util.List;

public class PairsImpairs {

	public static void main(String[] args) {
		
		int[] tab = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		
		System.out.println("Tableau avant reorganisation : ");
		System.out.print("|");
		for (int i=0; i<tab.length; i++) {
			System.out.print(" "+tab[i]+" |");
		}
		System.out.println("\n");
		
		pairsImpairs(tab);
		
		System.out.println("Tableau apres reorganisation : ");
		System.out.print("|");
		for (int i=0; i<tab.length; i++) {
			System.out.print(" "+tab[i]+" |");
		}
		System.out.println("\n");

	}
	
	/**
	 * Fonction qui reorganise un tableau d'entiers passe en parametre de telle maniere que tous les nombres pairs
	 * soient regroupes au debut du tableau et soient suivis des nombres impairs.
	 * @param tab
	 */
	public static void pairsImpairs(int[] tab) {
		List<Integer> pair = new ArrayList<Integer>();
		List<Integer> impair = new ArrayList<Integer>();
		for (int i=0; i<tab.length; i++) {
			if (tab[i]%2 == 0) {
				pair.add(new Integer(tab[i]));
			}
			else {
				impair.add(new Integer(tab[i]));
			}
		}
		for (int i=0; i<pair.size(); i++) {
			tab[i] = pair.get(i).intValue();
		}
		for (int i=0; i<impair.size(); i++) {
			tab[pair.size()+i] = impair.get(i).intValue();
		}
	}

}
