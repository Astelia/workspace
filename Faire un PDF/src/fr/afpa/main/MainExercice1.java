package fr.afpa.main;

import java.io.IOException;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import fr.afpa.objets.PDFGenerator;

public class MainExercice1 {

	public static void main(String[] args) {
		
		String chemin = "C:\\ENV\\Annexes\\LesPDFs\\Resultat\\ReproductionDocument.pdf";
		
		String afpa = "C:\\ENV\\Annexes\\LesPDFs\\Resultat\\afpaLogo.png";
		String samu = "C:\\ENV\\Annexes\\LesPDFs\\Resultat\\samu-15_1500_x_950.jpg";
		String police = "C:\\ENV\\Annexes\\LesPDFs\\Resultat\\police-secours17_1500_x_950.jpg";
		String pompiers = "C:\\ENV\\Annexes\\LesPDFs\\Resultat\\sapeur-pompiers18_1500_x_950.jpg";
		String sourd = "C:\\ENV\\Annexes\\LesPDFs\\Resultat\\appel-urgence-sourd-malentendant-114_1500_x_950.jpg";
		
		try {
			Document document = PDFGenerator.generateDocument(chemin, PageSize.A4.rotate());

			float scale = 0.14f;
			
			int y = 350;
			
			Image imageAfpa = PDFGenerator.generateBlockImage(afpa, 0.75f);
			Image imageSamu = PDFGenerator.generateBlockImage(samu, scale);
			Image imagePolice = PDFGenerator.generateBlockImage(police, scale);
			Image imagePompiers = PDFGenerator.generateBlockImage(pompiers, scale);
			Image imageSourd = PDFGenerator.generateBlockImage(sourd, scale);
			PDFGenerator.addImage(document, imageAfpa, 0, y+125);
			PDFGenerator.addImage(document, imageSamu, 0, y);
			PDFGenerator.addImage(document, imagePolice, 210, y);
			PDFGenerator.addImage(document, imagePompiers, 420, y);
			PDFGenerator.addImage(document, imageSourd, 630, y);
			
			String[] parag = {"CENTRE AFPA ROUBAIX", "20 Rue du Luxembourg - 59100 Roubaix", "LISTE DES SAUVETEURS SECOURISTES DU CENTRE"}; 
			Paragraph paragraphe = PDFGenerator.createParagraph(parag);
			paragraphe.setTextAlignment(TextAlignment.CENTER).setFontSize(20f);
			paragraphe.setFixedPosition(110, 250, 610); // 500 entre left et right
			
			PdfFont font = PdfFontFactory.createFont(StandardFonts.TIMES_BOLD);
			paragraphe.setFont(font);
			PDFGenerator.addParagraphe(document, paragraphe);
			
			Table table = new Table(2);
			table.setWidth(1000f);
			
			Cell cellNom = new Cell().add(new Paragraph("NOM/PRENOM").setTextAlignment(TextAlignment.CENTER).setFont(font));
			cellNom.setBackgroundColor(ColorConstants.GREEN).setWidth(600f);
			table.addHeaderCell(cellNom);
			Cell cellNum = new Cell().add(new Paragraph("n� t�l�phone").setTextAlignment(TextAlignment.CENTER).setFont(font));
			cellNum.setBackgroundColor(ColorConstants.GREEN).setWidth(1400f);
			table.addHeaderCell(cellNum);
			
			table.addCell("AKLIL MALIKA").setTextAlignment(TextAlignment.CENTER).setFont(font);
			table.addCell("06 81 74 81 32").setTextAlignment(TextAlignment.CENTER).setFont(font);
			
			table.addCell("ARNOUT CINDY").setTextAlignment(TextAlignment.CENTER).setFont(font);
			table.addCell("06 02 07 01 63").setTextAlignment(TextAlignment.CENTER).setFont(font);
			
			table.addCell("CHILAH DJAMILA").setTextAlignment(TextAlignment.CENTER).setFont(font);
			table.addCell("06 32 76 84 44").setTextAlignment(TextAlignment.CENTER).setFont(font);
			
			table.addCell("HUJEUX PASCAL").setTextAlignment(TextAlignment.CENTER).setFont(font);
			table.addCell("06 08 41 63 84").setTextAlignment(TextAlignment.CENTER).setFont(font);
			
			table.addCell("ROSSELLE NANCY").setTextAlignment(TextAlignment.CENTER).setFont(font);
			table.addCell("07 84 42 47 43").setTextAlignment(TextAlignment.CENTER).setFont(font);
			
			table.addCell("ZEHAR HADJ").setTextAlignment(TextAlignment.CENTER).setFont(font);
			table.addCell("06 32 76 65 37").setTextAlignment(TextAlignment.CENTER).setFont(font);
			
			document.add(table.setFixedPosition(30, 80, 770));
			document.close();
		}
		catch (NullPointerException e) {
			System.out.println("T'as ferm� le fichier pdf ?");
		} catch (IOException e) {
			System.out.println("?");
			e.printStackTrace();
		}
		
		
	}

}
