package fr.afpa.main;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.layout.Document;
import fr.afpa.objets.PDFGenerator;

public class MainBatman {

	public static void main(String[] args) {
		
		String chemin = "C:\\ENV\\Annexes\\LesPDFs\\test.pdf";
		String cheminImage = "C:\\ENV\\Annexes\\QRCode\\vCard.png";
		
		Document document = PDFGenerator.generateDocument(chemin, PageSize.A4);
		
		String[] noms = {"WAYNE", "Bruce"};
		
		//PDFGenerator.addParagraphe(document, noms);
		PDFGenerator.addImage(document, cheminImage, 400, 650);
		
		document.close();
		
	}

}
