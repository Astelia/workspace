package fr.afpa.objets;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;

import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;

public class PDFGenerator {

	
	// Cr�ation du document PDF
	public static Document generateDocument(String chemin, PageSize ps) {
		try { 
			PdfWriter writer = new PdfWriter(chemin); // G�n�re l'exception FileNotFoundException
			PdfDocument pdf = new PdfDocument(writer);
			return new Document(pdf, ps);
			
		} catch (FileNotFoundException e) {
			System.out.println("Le document n'existe pas !");
			//e.printStackTrace();
		}
		return null;
	}
	
	
	// Cr�er un paragraphe
	public static Paragraph createParagraph(String parag) {
		return new Paragraph(parag);
	}
	
	public static Paragraph createParagraph(String[] parags) {
		Paragraph paragraphe = new Paragraph();
		for (int i=0; i<parags.length; i++) {
			paragraphe.add(parags[i]+"\n");
		}
		return paragraphe;
	}
	
	
	// Ajouter un paragraphe � un document
	public static void addParagraphe(Document document, String parag) {
		document.add(new Paragraph("Cr�e le paragraphe d'un coup."));
		System.out.println("Paragraphe ajout� !");
	}
	
	public static void addParagraphe(Document document, String[] parags) {
		Paragraph paragraphe = new Paragraph();
		for (int i=0; i<parags.length; i++) {
			paragraphe.add(parags[i]+"\n");
		}
		document.add(paragraphe);
	}
	
	public static void addParagraphe(Document document, Paragraph paragraphe) {
		document.add(paragraphe);
	}
	
	
	// Modification de la mise en forme
	/*public static Paragraph paragrapheItalique() {
		
	}*/
	
	
	// Cr�er bloc image
	public static Image generateBlockImage(String chemin, float scale) {
		Image image;
		try {
			image = new Image(ImageDataFactory.create(chemin)).scale(scale, scale);
			return image;
		} catch (MalformedURLException e) {
			System.out.println("L'image n'existe pas.");
			//e.printStackTrace();
		}
		return null;
	}
	
	
	// Ajouter une image
	public static void addImage(Document document, String cheminImage) {
		try {
			Image image = new Image(ImageDataFactory.create(cheminImage));
			document.add(image);
		} catch (MalformedURLException e) {
			System.out.println("L'image n'existe pas.");
			//e.printStackTrace();
		}
	}
	
	public static void addImage(Document document, String cheminImage, int x, int y) {
		Image image;
		try {
			image = new Image(ImageDataFactory.create(cheminImage));
			document.add(image.setFixedPosition(x,y));
		} catch (MalformedURLException e) {
			System.out.println("L'image n'existe pas.");
			//e.printStackTrace();
		}
		
	}
	
	public static void addImage(Document document, Image image, int x, int y) {
		document.add(image.setFixedPosition(x,y));
	}
	
}
