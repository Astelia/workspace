package fr.afpa.main;

import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
public class TestSpreadSheet {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        
        // Tableau contenant tous les po�rateurs
        String[] tabOperation = new String[N];
        // Tableau contenant toutes les premieres operandes
        String[] tabArg1 = new String[N];
        // Tableau conteant toutes les secondes op�randes
        String[] tabArg2 = new String[N];
        // Tableau ci indique si une case a d�ja �t� calcul�e
        boolean[] tabVerif = new boolean[N];        
        
        for (int i = 0; i < N; i++) {
            tabOperation[i] = in.next();
            tabArg1[i] = in.next();
            tabArg2[i] = in.next();
        }
        
        int[] resultats = new int[N];
        for (int i=0; i<N; i++) {
            if (!tabVerif[i]) {
                resultats[i] = calcul(i, tabOperation[i], tabArg1[i], tabArg2[i], tabVerif, resultats
                                , tabOperation, tabArg1, tabArg2);
                tabVerif[i] = true;
            }
        }
        
        // Write an action using System.out.println() 
        // To debug: System.err.println("Debug messages...");
        for (int i = 0; i < N; i++) {
            System.out.println(resultats[i]);
        }
    }
    
    
    public static int parse(int i, String arg, boolean[] verif, int[] resultat, String[] op
                                        , String[] arg1, String[] arg2) {
        if (arg.charAt(0) == '$') {
            int ref = ref(arg);
            if (verif[ref]) {
                return resultat[ref];
            }
            else {
                return calcul(i, op[ref], arg1[ref], arg2[ref], verif, resultat, op, arg1, arg2);
            }
        }
        else {
            verif[i] = true;
            return val(arg);
        }
    }
    
    // Fonction calcul : (String, String, String) -> int
    // Permet d'effectuer d'effectuer chaque type de calcul 
    public static int calcul(int i, String op, String arg1, String arg2, boolean[] verif
                        , int[] resultat, String[] tabOp, String[] tabArg1, String[] tabArg2) {
        int entier1 = parse(i, arg1, verif, resultat, tabOp, tabArg1, tabArg2);
        int entier2 = parse(i, arg2, verif, resultat, tabOp, tabArg1, tabArg2);
        
        int res = 0;
        switch (op) {
            case "VALUE" : res = value(entier1, entier2);
                        break;
            case "ADD" : res = add(entier1, entier2);
                        break;
            case "SUB" : res = sub(entier1, entier2);
                        break;
            case "MULT" : res = mult(entier1, entier2);
                        break;
        }
        verif[i] = true;
        return res;
    }
    
    
    // Fonction val : (String) -> int
    // Transforme une cha�ne de caract�res en sa valeur num�rique
    public static int val(String arg) {
        if ("_".equals(arg)) {
            return 0;
        }
        else {
            return Integer.parseInt(arg);
        }
    }
    
    // Fonction ref : (String, String[]) -> String
    // R�cup�re la r�f�rence de la case et retourne le contenu de celle-ci
    public static int ref(String arg) {
        String indiceCase = arg.substring(1, arg.length());
        return Integer.parseInt(indiceCase);
    }
    
    
    // Fonction value : (int, int) -> int
    // Retourne la valeur de arg1
    public static int value(int arg1, int arg2) {
        return arg1;
    }
    
    // Fonction add : (int, int) -> int
    // Retourne la somme de ses 2 arguments
    public static int add(int arg1, int arg2) {
        return arg1+arg2;
    }
    
    // Fonction sub : (int, int) -> int
    // Retourne la soustraction de ses 2 arguments
    public static int sub(int arg1, int arg2) {
        return arg1-arg2;
    }
    
    // Fonction mult : (int, int) -> int
    // Retourne le produit de ses 2 arguments
    public static int mult(int arg1, int arg2) {
        return arg1*arg2;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}