package fr.afpa.main;

import fr.afpa.objets.Radar;
import fr.afpa.objets.Voiture;

public class Main {

	public static void main(String[] args) {
		
		Voiture voiture1 = new Voiture("grise", "AT-345-789", 300);
		//System.out.println(voiture1);
		
		Voiture voiture2 = new Voiture("blanche", "RK-123-890", 190);
		//System.out.println(voiture2);
		
		Voiture voiture3 = new Voiture("noire", "GC-456-901", 200);
		//System.out.println(voiture3);
		
		Voiture[] grille = {voiture1, voiture2, voiture3};
		
		Radar radar = new Radar(180, grille);
		radar.course(grille);
	}

}
