package fr.afpa.objets;

public class Voiture {

	private String couleur;
	private String plaqueImmat;
	private int vitesseMax;
	private int vitesse;
	private boolean marche;
	
	private int banque;
	private int pts;
	
	
	public Voiture(String c, String p, int v) {
		couleur = c;
		plaqueImmat = p;
		vitesseMax = v;
		marche = false;
		vitesse = 0;
		banque = 50000;
		pts = 12;
	}
	
	// Procedure qui retire des points a la voiture.
	public void retirerPoints(int pt) {
		pts -= pt;
		System.out.println("La voiture "+couleur+" perd "+pt+" points !");
		if (pts <= 0) {
			System.out.print("La voiture "+couleur+" n'a plus de points ! ");
			System.out.println("Retrait de permis !");
		}
		else {
			System.out.println("Il reste "+pts+" points sur le permis de la voiture "+couleur+".");
		}
	}
	
	// Procedure qui ajoute des points a la voiture.
	public void ajouterPoints(int pt) {
		pts += pt;
	}
	
	// Procedure qui retire de l'argent a la voiture.
	public void payer(int montant) {
		banque -= montant;
		System.out.print("La voiture "+couleur+" paye "+montant+" euros. ");
		System.out.println("Le compte de la voiture "+couleur+" est a : "+banque+" euros.");
		if (banque < 0) {
			System.out.println("La voiture "+couleur+" est a decouvert !");
		}
		System.out.println();
	}
	
	
	// Fonction qui retourne le nobre de points de la voiture.
	public int getPoints() {
		return pts;
	}
	
	// Fonction qui retourne la vitesse actuelle de la voiture.
	public int getVitesse() {
		return vitesse;
	}
	
	// Fonction qui retourne la couleur de la voiture.
	public String getCouleur() {
		return couleur;
	}
	
	
	// Procedure qui fait demarrer le moteur de la voiture.
	public void demarrer() {
		if (marche) {
			System.out.println("La voiture "+couleur+" est deja en marche.");
			System.out.println();
		}
		else {
			marche = true;
			afficherDemarre();
		}
	}
	
	// Procedure qui fait accelerer la voiture d'une vitesse v.
	public void accelerer(int v) {
		if (!marche) {
			System.out.println("La voiture "+couleur+" n'a pas encore ete demarree !");
			System.out.println();
		}
		else if (vitesse+v > vitesseMax) {
			vitesse = vitesseMax;
			System.out.println("La voiture "+couleur+" ne peut pas aller plus vite que "+vitesseMax+" km/h.");
			System.out.println("Elle roule a une vitesse de "+vitesse+" km/h.");
		}
		else {
			vitesse += v;
			afficherAccelerer();
		}
	}
	
	// Procedure qui fait freiner la voiture et reduit sa vitesse a 0.
	public void freiner() {
		if (!marche) {
			System.out.println("La voiture "+couleur+" est deja a l'arret !");
			System.out.println();
		}
		else {
			vitesse = 0;
			afficherFreiner();
		}
	}
	
	// Procedure qui arrete le moteur de la voiture.
	public void arreterMoteur() {
		if (!marche) {
			System.out.println("La voiture "+couleur+" est deja a l'arret !");
			System.out.println();
		}
		else {
			vitesse = 0;
			marche = false;
			afficherArretMoteur();
		}
	}
	
	
	// Procedures d'affichages
	private void afficherDemarre() {
		System.out.println("La voiture "+couleur+" demarre !");
		System.out.println();
	}
	
	private void afficherAccelerer() {
		System.out.println("La voiture "+couleur+" accelere ! Elle roule a une vitesse de "+vitesse+" km/h.");
		System.out.println();
	}
	
	private void afficherFreiner() {
		System.out.println("La voiture "+couleur+" freine ! Sa vitesse est a une vitesse de "+vitesse+" km/h.");
		System.out.println();
	}
	
	private void afficherArretMoteur() {
		System.out.println("La voiture "+couleur+" s'arrete.");
		System.out.println();
	}
	
	@Override
	public String toString() {
		return "Voiture [couleur=" + couleur + ", plaqueImmat=" + plaqueImmat + ", vitesseMax=" + vitesseMax
				+ ", vitesse=" + vitesse + ", marche=" + marche + "]";
	}
}
