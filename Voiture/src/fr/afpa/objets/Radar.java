package fr.afpa.objets;

import java.util.Random;

public class Radar {

	private int vitesseLimite;
	
	public Radar(int vitesse, Voiture[] voitures) {
		vitesseLimite = vitesse;
	}
	
	
	// Procedure qui lance la course entre les voitures du tableau.
	public void course(Voiture[] voitures) {
		int i=1;
		demarrerToutesLesVoitures(voitures);
		Voiture v = voitures[0];
		while (true) {
			v.accelerer(aleatoireInferieur(16));
			if (scannerVoitures(v)) {
				break;
			}
			i++;
			if (i == voitures.length) {
				i = 0;
			}
			v = voitures[i];
		}
		amende(v);
	}
	
	// Fonction qui retourne Vrai si une voiture est en exc�s de vitesse et faux sinon.
	private boolean scannerVoitures(Voiture voiture) {
		if (voiture.getVitesse() > vitesseLimite) {
			return true;
		}
		else {
			return false;
		}
	}
	
	// Procedure qui determine quelle amende doit payer le chauffard.
	private void amende(Voiture voiture) {
		System.out.println("Exces de vitesse de la voiture "+voiture.getCouleur()+" !!");
		System.out.println();
		int vitesseEnTrop = Math.abs(voiture.getVitesse() - vitesseLimite);
		if (vitesseEnTrop <= 5) {
			voiture.retirerPoints(2);
			voiture.payer(100000);
		}
		else if (5<vitesseEnTrop && vitesseEnTrop<=10) {
			voiture.retirerPoints(1);
	 		voiture.payer(2);
		}
		else if (10<vitesseEnTrop && vitesseEnTrop<=15) {
			voiture.ajouterPoints(1);
		}
		else {
			voiture.retirerPoints(voiture.getPoints());
		}
	}
	
	// Procedure qui permet de demarrer toutes les voitures.
	private void demarrerToutesLesVoitures(Voiture[] voitures) {
		for (int i=0; i<voitures.length; i++) {
			voitures[i].demarrer();
		}
	}
	
	// Fonction qui retourne un nombre aleatoire inferieur a max.
	public static int aleatoireInferieur(int max) {
		return new Random().nextInt(max);
	}
}
