package fr.afpa.main;

import java.util.Scanner;

import fr.afpa.objets.Agenda;
import fr.afpa.objets.Formateur;
import fr.afpa.objets.Groupe;
import fr.afpa.objets.InfosPlus;
import fr.afpa.objets.PDFGenerator;
import fr.afpa.objets.Question;

public class Main {

	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		Formateur formateur = Question.questionFormateur(scanner);
		Groupe groupe = Question.questionGroupe(scanner);
		Agenda agenda = Question.questionAgenda(scanner);
		InfosPlus infos = Question.questionInfoPlus(scanner);
		
		PDFGenerator pdf = new PDFGenerator(formateur, groupe, agenda, infos);
		
		String chemin = Question.questionChemin(scanner);
		pdf.generatePDF(chemin);
		
		scanner.close();
	}
	
	
	

}
