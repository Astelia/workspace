package fr.afpa.objets;

import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;

public class InfosPlus {

	private InfoPlusSimple infoSimples;
	private InfosPlusTableaux infoTableaux;
	
	
	public InfosPlus(InfoPlusSimple infoS, InfosPlusTableaux infoT) {
		infoSimples = infoS;
		infoTableaux = infoT;
	}

	
	public Table generateInfoPlus() {
		Table table = new Table(2);
		//table.setWidth(2000);
		infoSimples.generateInfoPlusSimple(table);
		infoTableaux.generateInfosPlusTableaux(table);
		
		Cell cellGRN = new Cell().add(new Paragraph("GRN : 164"));
		cellGRN.setBorderBottom(Border.NO_BORDER);
		cellGRN.setBorderLeft(Border.NO_BORDER);
		cellGRN.setBorderRight(Border.NO_BORDER);
		table.addCell(cellGRN);
		
		Cell cell = new Cell().add(new Paragraph("Signature du formateur :")).setTextAlignment(TextAlignment.CENTER);
		cell.setBorderBottom(Border.NO_BORDER);
		cell.setBorderLeft(Border.NO_BORDER);
		cell.setBorderRight(Border.NO_BORDER);
		table.addCell(cell);
		return table;
	}
	

	public InfoPlusSimple getInfoSimples() {
		return infoSimples;
	}

	public void setInfoSimples(InfoPlusSimple infoSimples_) {
		infoSimples = infoSimples_;
	}

	public InfosPlusTableaux getInfoTableaux() {
		return infoTableaux;
	}

	public void setInfoTableaux(InfosPlusTableaux infoTableaux_) {
		infoTableaux = infoTableaux_;
	}


	@Override
	public String toString() {
		return "InfosPlus [infoSimples=" + infoSimples + ", infoTableaux=" + infoTableaux + "]";
	}
	
}
