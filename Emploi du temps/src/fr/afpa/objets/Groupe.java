package fr.afpa.objets;

import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;

public class Groupe {

	private String nom;
	private String dateDebut;
	private String dateFin;
	private String salle;
	
	
	public Groupe(String nom_, String dateD, String dateF, String salle_) {
		nom = nom_;
		dateDebut = dateD;
		dateFin = dateF;
		salle = salle_;
	}
	
	
	public Paragraph generateParagraphe() {
		Paragraph paragraphe = new Paragraph();
		paragraphe.add("Groupe "+nom+" du "+dateDebut+" au "+dateFin+"\n");
		paragraphe.add("Salle "+salle);
		paragraphe.setTextAlignment(TextAlignment.CENTER);
		return paragraphe;
	}
	

	public String getNom() {
		return nom;
	}


	public void setNom(String nom_) {
		nom = nom_;
	}


	public String getDateDebut() {
		return dateDebut;
	}


	public void setDateDebut(String dateDebut_) {
		dateDebut = dateDebut_;
	}


	public String getDateFin() {
		return dateFin;
	}


	public void setDateFin(String dateFin_) {
		dateFin = dateFin_;
	}


	public String getSalle() {
		return salle;
	}


	public void setSalle(String salle_) {
		salle = salle_;
	}


	@Override
	public String toString() {
		return "Groupe [nom=" + nom + ", dateDebut=" + dateDebut + ", dateFin=" + dateFin + ", salle=" + salle + "]";
	}
	
	
	
	
}
