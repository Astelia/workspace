package fr.afpa.objets;

import java.util.Arrays;

import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;

public class InfosPlusTableaux {

	private String[] dialogues;
	private String[] conges;
	private String[] entreprise1;
	private String[] entreprise2;
	private String[] entreprise3;
	private String[] certification;
	
	
	public InfosPlusTableaux(String[] diag, String[] con, String[] ent1, String[] ent2, String[] ent3, String[] cer) {
		dialogues = diag;
		conges = con;
		entreprise1 = ent1;
		entreprise2 = ent2;
		entreprise3 = ent3;
		certification = cer;
	}

	
	public void generateInfosPlusTableaux(Table table) {
		tabDiag(table);
		tabConges(table);
		tabEnt1(table);
		tabEnt2(table);
		tabEnt3(table);
		tabCert(table);
	}

	private void tabDiag(Table table) {
		table.addCell("Espaces de dialogue :");
		Cell cell = new Cell();
		if (dialogues.length == 4) {
			cell.add(new Paragraph("du "+dialogues[0]+" au "+dialogues[1]+" - du "+dialogues[2]+" au "+dialogues[3]));
		}
		else if (dialogues.length == 2) {
			cell.add(new Paragraph("du "+dialogues[0]+" au "+dialogues[1]));
		}
		table.addCell(cell);
	}
	
	private void tabConges(Table table) {
		table.addCell("Conges : ");
		Cell cell = new Cell();
		if (conges.length == 4) {
			cell.add(new Paragraph("du "+conges[0]+" au "+conges[1]+" - du "+conges[2]+" au "+conges[3]));
		}
		else if (conges.length == 2) {
			cell.add(new Paragraph("du "+conges[0]+" au "+conges[1]));
		}
		table.addCell(cell);
	}
	
	private void tabEnt1(Table table) {
		table.addCell("Periode en Entreprise 1 :");
		Cell cell = new Cell();
		if (entreprise1.length == 2) {
			cell.add(new Paragraph("du "+entreprise1[0]+" au "+entreprise1[1]));
		}
		table.addCell(cell);
	}
	
	private void tabEnt2(Table table) {
		table.addCell("Periode en Entreprise 2 : ");
		Cell cell = new Cell();
		if (entreprise2.length == 2) {
			cell.add(new Paragraph("du "+entreprise2[0]+" au "+entreprise2[1]));
		}
		table.addCell(cell);
	}
	
	private void tabEnt3(Table table) {
		table.addCell("Periode en Entreprise 3 : ");
		Cell cell = new Cell();
		if (entreprise3.length == 2) {
			cell.add(new Paragraph("du "+entreprise3[0]+" au "+entreprise3[1]));
		}
		table.addCell(cell);
	}
	
	private void tabCert(Table table) {
		table.addCell("Certification : ");
		Cell cell = new Cell();
		if (certification.length == 2) {
			cell.add(new Paragraph("du "+certification[0]+" au "+certification[1]));
		}
		table.addCell(cell);
	}
	
	
	public String[] getDialogues() {
		return dialogues;
	}

	public void setDialogues(String[] dialogues_) {
		dialogues = dialogues_;
	}

	public String[] getConges() {
		return conges;
	}

	public void setConges(String[] conges_) {
		conges = conges_;
	}

	public String[] getEntreprise1() {
		return entreprise1;
	}

	public void setEntreprise1(String[] entreprise1_) {
		entreprise1 = entreprise1_;
	}

	public String[] getEntreprise2() {
		return entreprise2;
	}

	public void setEntreprise2(String[] entreprise2_) {
		entreprise2 = entreprise2_;
	}

	public String[] getEntreprise3() {
		return entreprise3;
	}

	public void setEntreprise3(String[] entreprise3_) {
		entreprise3 = entreprise3_;
	}

	public String[] getCertification() {
		return certification;
	}

	public void setCertification(String[] certification_) {
		certification = certification_;
	}


	@Override
	public String toString() {
		return "InfosPlusTableaux [dialogues=" + Arrays.toString(dialogues) + ", conges=" + Arrays.toString(conges)
				+ ", entreprise1=" + Arrays.toString(entreprise1) + ", entreprise2=" + Arrays.toString(entreprise2)
				+ ", entreprise3=" + Arrays.toString(entreprise3) + ", certification=" + Arrays.toString(certification)
				+ "]";
	}
	
}
