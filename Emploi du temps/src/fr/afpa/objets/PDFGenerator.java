package fr.afpa.objets;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;

import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Table;

public class PDFGenerator {

	private Formateur formateur;
	private Groupe groupe;
	private Agenda agenda;
	private InfosPlus infoPlus;
	
	
	public PDFGenerator(Formateur form, Groupe gr, Agenda ag, InfosPlus ip) {
		formateur = form;
		groupe = gr;
		agenda = ag;
		infoPlus = ip;
	}

	
	public void generatePDF(String chemin) {
		try {
			PdfWriter writer = new PdfWriter(chemin);
			PdfDocument pdf = new PdfDocument(writer);
			Document document = new Document(pdf, PageSize.A4.rotate());
			document.setFontSize(10);
			
			Table blocDebut = formGroIms();
			blocDebut.setFixedPosition(20, 490, 800);
			document.add(blocDebut);
			
			Table blocAgenda = agenda.generateTable();
			blocAgenda.setFixedPosition(20, 310, 800);
			document.add(blocAgenda);
			
			Table blocInfoPlus = infoPlus.generateInfoPlus();
			blocInfoPlus.setFixedPosition(20, 97, 700);
			document.add(blocInfoPlus);
			
			Image image = new Image(ImageDataFactory.create(formateur.getQrCode()));
			image.setFixedPosition(490, 3);
			document.add(image);
			
			document.close();
		} catch (FileNotFoundException e) {
			System.out.println("Le document n'existe pas !");
			//e.printStackTrace();
		} catch (MalformedURLException e) {
			System.out.println("!");
			//e.printStackTrace();
		}
		
	}
	
	private Table formGroIms() {
		Table table = new Table(3);
		
		Cell cellFormateur = new Cell().add(formateur.generateParagraphe());
		table.addCell(cellFormateur.setBorder(Border.NO_BORDER));
		
		Cell cellGroupe = new Cell().add(groupe.generateParagraphe()); 
		table.addCell(cellGroupe.setBorder(Border.NO_BORDER));
		
		Image image = generateLogo("C:\\ENV\\Workspace\\Emploi du temps\\Logos\\logo.bmp");
		Cell cellImage = new Cell().add(image);
		table.addCell(cellImage.setBorder(Border.NO_BORDER));
		return table;
	}
	
	private Image generateLogo(String cheminImage) {
		try {
			return new Image(ImageDataFactory.create(cheminImage));
		} catch (MalformedURLException e) {
			System.out.println("C'est le bon chemin de l'image ?");
			//e.printStackTrace();
		}
		return null;
	}
	

	public Formateur getFormateur() {
		return formateur;
	}


	public void setFormateur(Formateur formateur_) {
		formateur = formateur_;
	}


	public Groupe getGroupe() {
		return groupe;
	}


	public void setGroupe(Groupe groupe_) {
		groupe = groupe_;
	}


	public Agenda getAgenda() {
		return agenda;
	}


	public void setAgenda(Agenda agenda_) {
		agenda = agenda_;
	}


	public InfosPlus getInfoPlus() {
		return infoPlus;
	}


	public void setInfoPlus(InfosPlus infoPlus_) {
		infoPlus = infoPlus_;
	}


	@Override
	public String toString() {
		return "PDFGenerator [formateur=" + formateur + ", groupe=" + groupe + ", agenda=" + agenda + ", infoPlus="
				+ infoPlus + "]";
	}
	
	
	
	
}
