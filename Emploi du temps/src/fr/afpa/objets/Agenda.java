package fr.afpa.objets;

import java.util.Arrays;

import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;

public class Agenda {

	private String titre;
	private String[] lundi;
	private String[] mardi;
	private String[] mercredi;
	private String[] jeudi;
	private String[] vendredi;
	private String[] duree;
	
	
	public Agenda(String titre_, String[] lu, String[] ma, String[] me, String[] je, String[] ve, String[] duree_) {
		titre = titre_;
		lundi = lu;
		mardi = ma;
		mercredi = me;
		jeudi = je;
		vendredi = ve;
		duree = duree_;
	}

	
	/*private int calculTotalDuree() {
		int res = 0;
		for (int i=0; i<duree.length; i++) {
			res += Integer.parseInt(duree[i].substring(0, duree.length-1));
		}
		return res;
	}*/

	public Table generateTable() {
		Table table = new Table(7);
		addHeader(table);
		addLineMatinAprem(table);
		addLineDebutFin(table);
		
		table.addCell("LUNDI").setTextAlignment(TextAlignment.CENTER);
		addLineJour(table, lundi);
		table.addCell(duree[0]).setTextAlignment(TextAlignment.CENTER);
		
		table.addCell("MARDI").setTextAlignment(TextAlignment.CENTER);
		addLineJour(table, mardi);
		table.addCell(duree[1]).setTextAlignment(TextAlignment.CENTER);
		
		table.addCell("MERCREDI").setTextAlignment(TextAlignment.CENTER);
		addLineJour(table, mercredi);
		table.addCell(duree[2]).setTextAlignment(TextAlignment.CENTER);
		
		table.addCell("JEUDI").setTextAlignment(TextAlignment.CENTER);
		addLineJour(table, jeudi);
		table.addCell(duree[3]).setTextAlignment(TextAlignment.CENTER);
		
		table.addCell("VENDREDI").setTextAlignment(TextAlignment.CENTER);
		addLineJour(table, vendredi);
		table.addCell(duree[4]).setTextAlignment(TextAlignment.CENTER);
		
		Cell cell = new Cell(1,7).add(new Paragraph(duree[5]));
		cell.setBorder(Border.NO_BORDER);
		table.addCell(cell.setTextAlignment(TextAlignment.RIGHT));
		return table;
	}
	
	private void addHeader(Table table) {
		Cell cellTitre = new Cell(1,7).add(new Paragraph(titre));
		table.addHeaderCell(cellTitre.setTextAlignment(TextAlignment.CENTER));
	}
	
	private void addLineMatinAprem(Table table) {
		Cell cellPeriode = new Cell(2,1).add(new Paragraph("Periode"));
		table.addCell(cellPeriode.setTextAlignment(TextAlignment.CENTER));
		Cell cellMatin = new Cell(1,2).add(new Paragraph("MATIN"));
		table.addCell(cellMatin.setTextAlignment(TextAlignment.CENTER));
		table.addCell(new Cell());
		Cell cellApresMidi = new Cell(1,2).add(new Paragraph("APRES MIDI"));
		table.addCell(cellApresMidi.setTextAlignment(TextAlignment.CENTER));
		Cell cellDureeQuotidienne = new Cell(2,1).add(new Paragraph("Duree quotidienne")).setWidth(70);
		table.addCell(cellDureeQuotidienne.setTextAlignment(TextAlignment.CENTER));
	}
	
	private void addLineDebutFin(Table table) {
		Cell cellDebut = new Cell().add(new Paragraph("DEBUT")).setTextAlignment(TextAlignment.CENTER);
		Cell cellFin = new Cell().add(new Paragraph("FIN")).setTextAlignment(TextAlignment.CENTER);
		table.addCell(cellDebut);
		table.addCell(cellFin);
		table.addCell(new Cell());
		table.addCell(cellDebut);
		table.addCell(cellFin);
	}
	
	private void addLineJour(Table table, String[] tab) {
		table.addCell(tab[0]).setTextAlignment(TextAlignment.CENTER);
		table.addCell(tab[1]).setTextAlignment(TextAlignment.CENTER);
		table.addCell(new Cell());
		table.addCell(tab[2]).setTextAlignment(TextAlignment.CENTER);
		table.addCell(tab[3]).setTextAlignment(TextAlignment.CENTER);
	}
	
	
	/*private void addLineTotalDuree(Table table) {
		Cell cell = new Cell(1,7).add(new Paragraph(""+calculTotalDuree()));
		table.addCell(cell.setTextAlignment(TextAlignment.RIGHT));
	}*/
	
	public boolean verifTabJour(String[] tab) {
		return tab.length == 4;
	}
	
	public boolean verifTabDuree(String[] tab) {
		return tab.length == 5;
	}
	
	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre_) {
		titre = titre_;
	}

	public String[] getLundi() {
		return lundi;
	}

	public void setLundi(String[] lundi_) {
		lundi = lundi_;
	}

	public String[] getMardi() {
		return mardi;
	}

	public void setMardi(String[] mardi_) {
		mardi = mardi_;
	}

	public String[] getMercredi() {
		return mercredi;
	}

	public void setMercredi(String[] mercredi_) {
		mercredi = mercredi_;
	}

	public String[] getJeudi() {
		return jeudi;
	}

	public void setJeudi(String[] jeudi_) {
		jeudi = jeudi_;
	}

	public String[] getVendredi() {
		return vendredi;
	}

	public void setVendredi(String[] vendredi_) {
		vendredi = vendredi_;
	}


	public String[] getDuree() {
		return duree;
	}


	public void setDuree(String[] duree_) {
		duree = duree_;
	}


	@Override
	public String toString() {
		return "Agenda [titre=" + titre + ", lundi=" + Arrays.toString(lundi) + ", mardi=" + Arrays.toString(mardi)
				+ ", mercredi=" + Arrays.toString(mercredi) + ", jeudi=" + Arrays.toString(jeudi) + ", vendredi="
				+ Arrays.toString(vendredi) + ", duree=" + Arrays.toString(duree) + "]";
	}
	
	
		
}
