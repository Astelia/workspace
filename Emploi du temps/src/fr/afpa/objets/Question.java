package fr.afpa.objets;

import java.util.Scanner;

public class Question {
	
	public static String questionChemin(Scanner scanner) {
		System.out.print("Indiquer le chemin du dossier dans lequel enregistrer le pdf : ");
		 return scannerPhrase(scanner);
	}

	public static Groupe questionGroupe(Scanner scanner) {
		System.out.print("Entrer nom du groupe : ");
		String nom = scannerMot(scanner);
		
		System.out.print("Entrer date de debut de la formation : ");
		String dateDebut = scannerMot(scanner);
		
		System.out.print("Entrer date de fin de la formation : ");
		String dateFin = scannerMot(scanner);
		
		System.out.print("Entrer numero de la salle : ");
		String salle = scannerMot(scanner);
		
		return new Groupe(nom, dateDebut, dateFin, salle);
	}
	
	
	public static Formateur questionFormateur(Scanner scanner) {
		System.out.print("Entrer nom du formateur : ");
		String nom = scannerMot(scanner);
		
		System.out.print("Entrer prenom du formateur : ");
		String prenom = scannerMot(scanner);
		
		System.out.print("Entrer numero de telephone du formateur : ");
		String telephone = scannerPhrase(scanner);
		
		System.out.print("Entrer numero de fax : ");
		String fax = scannerPhrase(scanner);
		
		System.out.print("Entrer chemin image QRCode : ");
		String qrCode = scannerPhrase(scanner);
		
		return new Formateur(nom, prenom, telephone, fax, qrCode);
	}
	
	
	public static Agenda questionAgenda(Scanner scanner) {
		System.out.print("Entrer l'intitule de la formation : ");
		String titre = scannerPhrase(scanner);
		
		String[] tabLundi = questionHoraires(scanner, "lundi");
		String[] tabMardi = questionHoraires(scanner, "mardi");
		String[] tabMercredi = questionHoraires(scanner, "mercredi");
		String[] tabJeudi = questionHoraires(scanner, "jeudi");
		String[] tabVendredi = questionHoraires(scanner, "vendredi");
		
		String[] tabDuree = new String[6];
		System.out.print("Entrer duree des cours chaque jour : ");
		for (int i=0; i<5; i++) {
			tabDuree[i] = scannerMot(scanner);
		}
		System.out.print("Entrer duree totale des cours : ");
		tabDuree[5] = scannerMot(scanner);
		return new Agenda(titre, tabLundi, tabMardi, tabMercredi, tabJeudi, tabVendredi, tabDuree);
	}

	private static String[] questionHoraires(Scanner scanner, String jour) {
		System.out.print("Entrer les horaires du "+jour+" : ");
		String[] tabJour = new String[4];
		for (int i=0; i<4; i++) {
			tabJour[i] = scannerMot(scanner);
		}
		return tabJour;
	}
	
	
	public static InfosPlus questionInfoPlus(Scanner scanner) {
		return new InfosPlus(questionInfoPlusSimple(scanner), questionInfosPlusTableaux(scanner));
	}
	
	private static InfosPlusTableaux questionInfosPlusTableaux(Scanner scanner) {
		System.out.print("Entrer dates des espaces de dialogues : ");
		String[] datesEspaces = infoTab(scanner, 4);
		
		System.out.print("Entrer dates de conges : ");
		String[] datesConges = infoTab(scanner, 4);
		
		System.out.print("Entrer dates periode en entreprise 1 : ");
		String[] datesEnt1 = infoTab(scanner, 2);
		
		System.out.print("Entrer dates periode en entreprise 2 : ");
		String[] datesEnt2 = infoTab(scanner, 2);
		
		System.out.print("Entrer dates periode en entreprise 3 : ");
		String[] datesEnt3 = infoTab(scanner, 2);
		
		System.out.print("Entrer dates certification : ");
		String[] datesCert = infoTab(scanner, 2);
		
		return new InfosPlusTableaux(datesEspaces, datesConges, datesEnt1, datesEnt2, datesEnt3, datesCert);
	}
	
	private static String[] infoTab(Scanner scanner, int nbLignes) {
		String[] tab = new String[nbLignes];
		for (int i=0; i<nbLignes; i++) {
			tab[i] = scannerMot(scanner);
		}
		return tab;
	}
	
	private static InfoPlusSimple questionInfoPlusSimple(Scanner scanner) {
		System.out.print("Entrer date election : ");
		String election = scannerMot(scanner);
		
		System.out.print("Entrer date rencontre : ");
		String rencontre = scannerMot(scanner);
		
		System.out.print("Entrer date sensibilisation : ");
		String sens = scannerMot(scanner);
		
		System.out.print("Entrer date atelier : ");
		String atelier = scannerMot(scanner);
		
		return new InfoPlusSimple(election, rencontre, sens, atelier);
	}
	
	
	private static String scannerMot(Scanner scanner) {
		String mot = scanner.next();
		scanner.nextLine();
		return mot;
	}
	
	private static String scannerPhrase(Scanner scanner) {
		return scanner.nextLine();
	}
}
