package fr.afpa.objets;

import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;

public class InfoPlusSimple {

	private String election;
	private String rencontre;
	private String sensibilisation;
	private String atelier;
	
	
	public InfoPlusSimple(String elec, String ren, String sen, String ate) {
		election = elec;
		rencontre = ren;
		sensibilisation = sen;
		atelier = ate;
	}

	
	public void generateInfoPlusSimple(Table table) {
		Cell cellTitre = new Cell().add(new Paragraph("Election delegues stagiaires, le :")).setWidth(700);
		table.addCell(cellTitre);
		Cell cell = new Cell().add(new Paragraph(election)).setWidth(750);
		table.addCell(cell);
		
		table.addCell("Rencontre avec le DC, le MF, le :");
		table.addCell(rencontre);
		
		table.addCell("Sensibilisation au Developpement Durable, le :");
		table.addCell(sensibilisation);
		
		table.addCell("Atelier Accompagnement Vers l'Emploi, le :");
		table.addCell(atelier);
	}
	

	public String getElection() {
		return election;
	}

	public void setElection(String election_) {
		election = election_;
	}

	public String getRencontre() {
		return rencontre;
	}

	public void setRencontre(String rencontre_) {
		rencontre = rencontre_;
	}

	public String getSensibilisation() {
		return sensibilisation;
	}

	public void setSensibilisation(String sensibilisation_) {
		sensibilisation = sensibilisation_;
	}

	public String getAtelier() {
		return atelier;
	}

	public void setAtelier(String atelier_) {
		atelier = atelier_;
	}


	@Override
	public String toString() {
		return "InfoPlusSimple [election=" + election + ", rencontre=" + rencontre + ", sensibilisation="
				+ sensibilisation + ", atelier=" + atelier + "]";
	}	
	
}
