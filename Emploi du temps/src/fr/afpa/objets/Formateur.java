package fr.afpa.objets;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.itextpdf.layout.element.Paragraph;

public class Formateur {

	private String nom;
	private String prenom;
	private String telephone;
	private String fax;
	private String qrCode;
	
	
	public Formateur(String nom_, String prenom_, String telephone_, String fax_, String chemin) {
		nom = nom_;
		prenom = prenom_;
		telephone = telephone_;
		fax = fax_;
		qrCode = chemin;
		generateQRCode();
	}

	
	public Paragraph generateParagraphe() {
		Paragraph paragraphe = new Paragraph();
		paragraphe.add("Centre AFPA de Roubaix\n");
		paragraphe.add("");
		paragraphe.add("");
		paragraphe.add("Formateur : "+nom+" "+prenom+"\n");
		paragraphe.add("Tel. : "+telephone+"\n");
		paragraphe.add("Fax : "+fax);
		return paragraphe;
	}
	
	private void generateQRCode() {
		try {
			BitMatrix phraseCodee = new QRCodeWriter().encode(nom+"\n"+prenom, BarcodeFormat.QR_CODE, 100, 100);
			FileOutputStream fluxDeSortie = new FileOutputStream(new File(qrCode));
			MatrixToImageWriter.writeToStream(phraseCodee, "png", fluxDeSortie);
			fluxDeSortie.close();
		} catch (WriterException e) {
			System.out.println("L'image a bien ete encodee ?");
			//e.printStackTrace();
		} catch (FileNotFoundException e) {
			System.out.println("Le fichier n'existe pas.");
			//e.printStackTrace();
		} catch (IOException e) {
			System.out.println("?");
			//e.printStackTrace();
		}
	}
	
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom_) {
		nom = nom_;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom_) {
		prenom = prenom_;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone_) {
		telephone = telephone_;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax_) {
		fax = fax_;
	}


	public String getQrCode() {
		return qrCode;
	}


	public void setQrCode(String qrCode_) {
		qrCode = qrCode_;
	}


	@Override
	public String toString() {
		return "Formateur [nom=" + nom + ", prenom=" + prenom + ", telephone=" + telephone + ", fax=" + fax
				+ ", qrCode=" + qrCode + "]";
	}
	
}
