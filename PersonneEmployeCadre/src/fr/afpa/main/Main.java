package fr.afpa.main;

import fr.afpa.objets.Cadre;
import fr.afpa.objets.Employe;
import fr.afpa.objets.Personne;

public class Main {

	public static void main(String[] args) {
		
		Personne p1 = new Personne("Grayson", "Richard");
		p1.sePresenter();
		System.out.println();
		
		Personne p2 = new Employe("Drake", "Tim", "0003", 1500f);
		p2.sePresenter();
		System.out.println();
		
		Personne p3 = new Cadre("Wayne", "Damian", "0002", 2000f, "Fils du patron");
		p3.sePresenter();
		System.out.println();
		
		//Cadre cadre = new Cadre("Wayne", "Bruce", "0000", 10000f, "Big Boss");
		//cadre.sePresenter();

	}

}
