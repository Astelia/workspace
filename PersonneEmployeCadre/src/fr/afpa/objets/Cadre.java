package fr.afpa.objets;

public class Cadre extends Employe {

	private String service;

	
	public Cadre(String nom, String prenom, String numEmploye, float salaire, String service) {
		super(nom, prenom, numEmploye, salaire);
		this.service = service;
	}

	
	public void sePresenter() {
		super.sePresenter();
		System.out.println("[service=" + service + "]");
	}
	
	public float calculPrime() {
		return super.calculPrime()/2;
	}
	
	
	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}


	@Override
	public String toString() {
		return "Cadre [service=" + service + "]";
	}
	
}
