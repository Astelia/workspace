package fr.afpa.objets;

public class Employe extends Personne {

	private String numEmploye;
	private float salaire;
	
	
	public Employe(String nom, String prenom, String numEmploye, float salaire) {
		super(nom, prenom);
		this.numEmploye = numEmploye;
		this.salaire = salaire;
	}

	
	public void sePresenter() {
		super.sePresenter();
		System.out.println("[numEmploye=" + numEmploye + ", salaire=" + salaire + "]");
	}
	
	public float calculPrime() {
		return this.salaire * 0.05f;
	}
	
	
	public String getNumEmploye() {
		return numEmploye;
	}

	public void setNumEmploye(String numEmploye) {
		this.numEmploye = numEmploye;
	}

	public float getSalaire() {
		return salaire;
	}

	public void setSalaire(float salaire) {
		this.salaire = salaire;
	}


	@Override
	public String toString() {
		return "Employe [numEmploye=" + numEmploye + ", salaire=" + salaire + "]";
	}
	
	
	
}
