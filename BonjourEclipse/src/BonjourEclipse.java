import java.util.Scanner;

public class BonjourEclipse {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		/*
		System.out.print("Entrer un mot : ");
		String mot = scanner.next();
		scanner.nextLine();
		
		String tom = reverse(mot);
		
		System.out.println(tom);
		System.out.println();
		
		par3(mot);
		*/
		
		System.out.print("Entrer un nombre : ");
		int n = scanner.nextInt();
		scanner.nextLine();
		triangle(n);
		
		scanner.close();
	}
	
	
	
	public static String reverse(String mot) {
		String res = "";
		
		for (int i=0; i<mot.length(); i++) {
			res = mot.charAt(i) + res;
		}
		
		return res;
	}
	
	
	public static void par3(String mot) {
		int i =0;
		for (; i < mot.length()/3; i++) {
			System.out.println(mot.substring(i*3, i*3+3));
		}
		System.out.println(mot.substring(i*3));
	}
	
	
//	  public static void par3(String str) {
//	  		for (int i=0; i<str.length(); i++) {
//	  			if (i%3 == 0 && i != 0) {
//	  				System.out.println();
//	  			}
//	  			System.out.print(str.charAt(i));
//	  		}
//	  }
	  
	
	public static void triangle(int n) {
		for (int i=1; i<=n; i++) {
			for (int j=1; j<=i; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}
