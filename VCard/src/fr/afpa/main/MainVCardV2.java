package fr.afpa.main;

import java.io.IOException;
import java.util.Scanner;

import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import fr.afpa.objets.QRCodeGenaratorUtilitaire;
import fr.afpa.objets.VCardV2;

public class MainVCardV2 {
	
	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) throws WriterException, IOException {
		
		String nomDuFichier = "C:\\ENV\\Annexes\\QRCode"+"\\"+"Myst�re"+"."+"png";
		
		System.out.print("Entrer nom : ");
		String nom = scanner.next();
		scanner.nextLine();
		
		System.out.print("Entrer pr�nom : ");
		String prenom = scanner.next();
		scanner.nextLine();
		
		System.out.print("Entrer nom associ� � la VCard : ");
		String fn = scanner.next();
		scanner.nextLine();
		
		System.out.print("Entrer adresse : ");
		String adr = scanner.next();
		scanner.nextLine();
		
		System.out.print("Entrer nom de l'entreprise : ");
		String work = scanner.next();
		scanner.nextLine();
		
		System.out.print("Entrer le num�ro de t�l�phone : ");
		String tel = scanner.next();
		scanner.nextLine();
		
		System.out.print("Entrer l'email : ");
		String email = scanner.next();
		scanner.nextLine();
		
		
		String phraseACoder = new VCardV2(fn, nom, prenom, adr, work, tel, email).createVCard();
		BitMatrix bm = QRCodeGenaratorUtilitaire.generateMatrix(phraseACoder, 300);
		QRCodeGenaratorUtilitaire.writeImage(bm, nomDuFichier);
		
		scanner.close();
	}

}
