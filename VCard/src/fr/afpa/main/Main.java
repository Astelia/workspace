package fr.afpa.main;

import java.io.IOException;
import java.util.Scanner;

import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import fr.afpa.objets.QRCodeGenerator;

public class Main {
	
	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) throws WriterException, IOException {

		System.out.print("Entrer la phrase � coder : ");
		String phrase = scannerString();
		
		System.out.print("Entrer la taille du QR Code : ");
		int taille = scannerEntier();
		
		
		System.out.print("Entrer le chemin o� sera g�n�r� le fichier : ");
		String chemin = scannerMot();
		
		System.out.print("Entrer le nom du futur fichier : ");
		String nom = scannerMot();
		
		System.out.print("Entrer le format d'image final : ");
		String format = scannerMot();
		
		
		QRCodeGenerator qr = new QRCodeGenerator(phrase, taille, chemin, nom, format);
		BitMatrix bm = qr.generateMatrix();
		qr.writeImage(bm);
		
		scanner.close();
	}

	
	private static String scannerString() {
		
		return scanner.nextLine();
	}
	
	private static String scannerMot() {
		String mot = scanner.next();
		scanner.nextLine();
		return mot;
	}
	
	private static int scannerEntier() {
		int entier = scanner.nextInt();
		scanner.nextLine();
		return entier;
	}
	
}
