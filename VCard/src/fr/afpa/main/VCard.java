package fr.afpa.main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class VCard {

	// Etape 0 : Importer ZXing.jar en cr�ant le nouveau projet.
	
	// Etape 1 : Ajouter la gestion des exceptions (throws WriterException, IOException). 
	
	public static void main(String[] args) throws WriterException, IOException {
		
		// Etape 
		/*Scanner scanner = new Scanner(System.in);
		System.out.print("Veuillez entrer la phrase � transformer en QR Code : ");
		String phrase = scanner.nextLine();*/
		String phrase = "BEGIN:VCARD\r\n" + 
						"VERSION:2.1\r\n" + 
						"FN:Bruce WAYNE\r\n" + 
						"N:WAYNE;Bruce\r\n" + 
						"ADR;WORK;PREF;QUOTED-PRINTABLE:;Gotham - USA;WayneTech\r\n" + 
						"LABEL;QUOTED-PRINTABLE;WORK;PREF:WayneTech 6A=Gotham 1200=USA\r\n" + 
						"TEL;CELL:+1234 56789\r\n" + 
						"EMAIL;INTERNET:bruce.wayne@waynetech.com\r\n" + 
						"UID:\r\n" + 
						"END:VCARD";
		
		BitMatrix phraseCodee = new QRCodeWriter().encode(phrase, BarcodeFormat.QR_CODE, 200, 200);
		
		String format = "png"; // Format de sortie du fichier.
		String cheminDuFichier = "C:\\ENV\\Annexes\\QRCode";
		String nomDuFichier = "vCard";
		
		FileOutputStream fluxDeSortie = new FileOutputStream(new File(cheminDuFichier+"\\"+nomDuFichier+"."+format));
		
		MatrixToImageWriter.writeToStream(phraseCodee, format, fluxDeSortie);
		
		fluxDeSortie.close();
		
	}

}
