package fr.afpa.main;

import java.util.Scanner;

import com.google.zxing.common.BitMatrix;

import fr.afpa.objets.QRCodeGenaratorUtilitaire;

public class MainUtilitaire {

	public static void main(String[] args) {
		
		String nomDuFichier = "C:\\ENV\\Annexes\\QRCo"+"\\"+"testUtilitaire"+"."+"png";
		
		System.out.print("Veuillez entrer la phrase � transformer en QR Code : ");
		Scanner scanner = new Scanner(System.in);
		String phrase = scanner.nextLine();
		
		System.out.print("Entrer la taille du QR Code : ");
		int taille = scanner.nextInt();
		scanner.nextLine();
		
		BitMatrix bm = QRCodeGenaratorUtilitaire.generateMatrix(phrase, taille);
		QRCodeGenaratorUtilitaire.writeImage(bm, nomDuFichier);
		
		scanner.close();
	}

}
