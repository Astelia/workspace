package fr.afpa.objets;

public class VCardV2 {

	/*String phrase = "BEGIN:VCARD\r\n" + 
			"VERSION:2.1\r\n" + 
			"FN:Bruce WAYNE\r\n" + 
			"N:WAYNE;Bruce\r\n" + 
			"ADR;WORK;PREF;QUOTED-PRINTABLE:;Gotham - USA;WayneTech\r\n" + 
			"LABEL;QUOTED-PRINTABLE;WORK;PREF:WayneTech 6A=Gotham 1200=USA\r\n" + 
			"TEL;CELL:+1234 56789\r\n" + 
			"EMAIL;INTERNET:bruce.wayne@waynetech.com\r\n" + 
			"UID:batman\r\n" + 
			"END:VCARD";*/
	
	private String fn;
	private String nNom;
	private String nPrenom;
	private String adr;
	private String work;
	private String tel;
	private String email;
	
	
	public VCardV2(String fn_, String nN_, String nP_, String adr_, String work_, String tel_, String email_) {
		fn = fn_;
		nNom = nN_;
		nPrenom = nP_;
		adr = adr_;
		work = work_;
		tel = tel_;
		email = email_;
	}
	
	
	public String createVCard() {
		return 	"BEGIN:VCARD\r\n" + 
				"VERSION:2.1\r\n" + 
				"FN:"+fn+"\r\n" + 
				"N:"+nNom+";"+nPrenom+"\r\n" + 
				"ADR;WORK;PREF;QUOTED-PRINTABLE:;"+adr+";"+work+"\r\n" + 
				"LABEL;QUOTED-PRINTABLE;WORK;PREF:"+work+" 6A="+adr+"\r\n" + 
				"TEL;CELL:"+tel+"\r\n" + 
				"EMAIL;INTERNET:"+email+"\r\n" + 
				"UID:\r\n" + 
				"END:VCARD";
	}
	
	
	
}
