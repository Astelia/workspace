package fr.afpa.objets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class QRCodeGenaratorUtilitaire {

	
	public static BitMatrix generateMatrix(String phraseACoder, int taille) {
		try {
			return new QRCodeWriter().encode(phraseACoder, BarcodeFormat.QR_CODE, taille, taille);
		}
		catch (WriterException we) {
			System.out.println("Soucis, il y a un WriterException !");
		}
		return null;
	}
	
	public static void writeImage(BitMatrix bm, String nomDuFichier) {
		try {
			FileOutputStream fluxDeSortie = new FileOutputStream(new File(nomDuFichier));
			MatrixToImageWriter.writeToStream(bm, "png", fluxDeSortie);
			fluxDeSortie.close();
		}
		catch (IOException ioe) {
			System.out.println("Ton fichier n'existe pas !");
		}
	}
	
	
}
