package fr.afpa.objets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class QRCodeGenerator {

	private String phraseACoder;
	private int taille;
	
	private String cheminDuFichier;
	private String nomDuFichier;
	private String format;
	
	
	public QRCodeGenerator(String phrase, int taille_, String chemin, String nom, String form) {
		phraseACoder = phrase;
		taille = taille_;
		cheminDuFichier = chemin;
		nomDuFichier = nom;
		format = form;
	}
	
	
	public BitMatrix generateMatrix() throws WriterException {
		return new QRCodeWriter().encode(phraseACoder, BarcodeFormat.QR_CODE, taille, taille);
	}
	
	private File creerFichier() {
		return new File(cheminDuFichier+"\\"+nomDuFichier+"."+format);
	}
	
	public void writeImage(BitMatrix bm) throws IOException {
		FileOutputStream fluxDeSortie = new FileOutputStream(creerFichier());
		MatrixToImageWriter.writeToStream(bm, format, fluxDeSortie);
		fluxDeSortie.close();
	}
	
}
