package fr.afpa.main;

import fr.afpa.objets.Ecrire;
import fr.afpa.objets.Lire;

public class Main {

	public static void main(String[] args) {
		
		// Exercice 1
		Ecrire.ecrireFichier("C:\\ENV\\Workspace\\Lecture Ecriture d'un Fichier\\Annexes\\testLectureEcriture.txt");
		
		// Exercice 2
		Lire.lireFichier("C:\\ENV\\Workspace\\Lecture Ecriture d'un Fichier\\Annexes\\testLectureEcriture.txt");
		System.out.println();
		
		// Exercice 3
		Lire.listerRepertoire("C:\\ENV");
	}

}
