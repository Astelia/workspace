package fr.afpa.objets;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Lire {

	public static void lireFichier(String fileName) {
		try {
			FileReader fr = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);
			while(br.ready()) {
				System.out.println(br.readLine());
			}
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("Est-ce que c'est le bon nom de fichier ?");
			//e.printStackTrace();
		} catch (IOException e) {
			System.out.println("?");
			//e.printStackTrace();
		}
		
	}
	
	
	public static void listerRepertoire(String repName) {
		File rep = new File(repName);
		if (rep.isDirectory()) {
			String tab[] = rep.list();
			for (int i=0; i<tab.length; i++) {
				System.out.println(tab[i]);
			}
		}
	}
	
	
}
