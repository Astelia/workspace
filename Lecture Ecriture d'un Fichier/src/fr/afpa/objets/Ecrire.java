package fr.afpa.objets;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Ecrire {

	public static void ecrireFichier(String fileName) {
		try {
			FileWriter fw = new FileWriter(fileName);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("Le Corbeau et le Renard");
			bw.newLine();
			bw.write("Le Lion et le Rat");
			bw.newLine();
			bw.write("Le Lievre et la Tortue");
			bw.close();
		} catch (IOException e) {
			System.out.println("Le dossier dans lequel doit etre ecrit le fichier n'existe pas !");
			//e.printStackTrace();
		}
		
		
	}
	
}
