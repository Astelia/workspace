package fr.afpa.objets;

public class Quoi {

	public Quoi() {
		
	}
	
	public void affichageInfosClient(Client client, CompteBancaire[] cb) {
		System.out.println("                            Fiche client                             ");
		System.out.println();
		System.out.println("Numero client : "+client.getIdentifiant());
		System.out.println("Nom : "+client.getNom());
		System.out.println("Prenom : "+client.getPrenom());
		System.out.println("Date de naissance : "+client.getdateaissance());
		System.out.println();
		System.out.println("_____________________________________________________________________");
		System.out.println("Liste de compte");
		System.out.println("_____________________________________________________________________");
		System.out.println("Numero de compte                      Solde");
		for (int i=0; i<cb.length; i++) {
			
			System.out.println(cb[i].getNumeroCompte()+"                       "+cb[i].getSolde()+"               "+smiley(cb[i].getSolde()));
		}
	}
	
	private String smiley(float solde) {
		if (solde < 0) {
			return "(TT_TT)";
		}
		else {
			return ":-)";
		}
	}
	
	
	public void affichageMenu() {
		System.out.println("Menu :");
		System.out.println();
		System.out.println("1- Creer une agence");
		System.out.println("2- Creer un client");
		System.out.println("3- Creer un compte bancaire");
		System.out.println("4- Recherche de compte (numero de compte)");
		System.out.println("5- Recherche de client (Nom, Numero de compte, identifiant de client)");
		System.out.println("6- Afficher la liste des comptes d'un client (identifiant client)");
		System.out.println("7- Imprimer les infos client (identifiant client)");
		System.out.println("8- Quitter le programme");
	}
	
}
