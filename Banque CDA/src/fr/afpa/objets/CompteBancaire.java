package fr.afpa.objets;

public class CompteBancaire {

	private String numeroCompte;
	private String codeAgence;
	private Client client;
	private float solde;
	private boolean decouvert;

	public CompteBancaire(String n, String c, Client cl, float s, boolean d) {

		numeroCompte = n;
		codeAgence = c;
		client = cl;
		solde = s;
		decouvert = d;
	}
	
	public void setNumeroCompte(String n) {
		if (n.length() == 11) {
			int i=0;
			while (i<n.length() && Character.isDigit(n.charAt(i))) {
				i++;
			}
			if (i == 11) {
				numeroCompte = n;
			}
		}
	}
	
	public String getNumeroCompte() {
		return numeroCompte;
	}
	
	
	public void setCodeAgence(String cA) {
		codeAgence = cA;
	}
	
	public String getCodeAgence() {
		return codeAgence;
	}
	
	public void setClient(Client c) {
		client = c;
	}
	
	public Client getClient() {
		return client;
	}
	
	public void setSolde(float s) {
		solde = s;
	}
	
	public float getSolde() {
		return solde;
	}
	
	public void setDecouvert(boolean d) {
		decouvert = d;
	}

}
