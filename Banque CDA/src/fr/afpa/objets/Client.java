package fr.afpa.objets;

public class Client {

	private String identifiant;
	private String nom;
	private String prenom;
	private String dateNaissance;
	private String email;

	public Client(String i, String n, String p, String dN, String e) {

		setIdentifiant(i);
		setNom(n);
		setPrenom(p);
		setDateNaissance(dN);
		setEmail(e);

	}

	public void setIdentifiant(String id) {
		if (id.length() == 8) {
			int i = 0;
			while (i < 2 && Character.isUpperCase(id.charAt(i))) {
				i++;
			}
			if (i == 2) {
				while (i < id.length() && Character.isDigit(id.charAt(i))) {
					i++;
				}
				if (i == 8) {
					identifiant = id;
				}
			}
		}
	}

	public String getIdentifiant() {
		return identifiant;
	}

	public void setNom(String n) {
		nom = n;
	}

	public String getNom() {
		return nom;
	}

	public void setPrenom(String p) {
		prenom = p;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setDateNaissance(String dN) {
		dateNaissance = dN;
	}

	public String getdateaissance() {
		return dateNaissance;
	}

	public void setEmail(String e) {
		email = e;
	}

	public String getEmail() {
		return email;
	}
}
