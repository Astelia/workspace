package fr.afpa.objets;

public class Agence {

	private String codeAgence;
	private String nom;
	private String adresse;

	public Agence(String cA, String n, String a) {

		setCodeAgence(cA);
		setNom (n);
		setAdresse (a);
	}

	public void setCodeAgence(String cA) {
		if (cA.length() == 3) {
			int i = 0;
			while (i < 3 && Character.isDigit(cA.charAt(i))) {
				i++;
			}
			if (i == 3) {
				codeAgence = cA;
			}
		}
	}

	public String getCodeAgence() {
		return codeAgence;
	}

	public void setNom(String n) {
		nom = n;
	}

	public String getNom() {
		return nom;
	}

	public void setAdresse(String a) {
		adresse = a;
	}

	public String getAdresse() {
		return adresse;
	}

}
