package fr.afpa.main;

import fr.afpa.objets.Personne;

public class Main {

	public static void main(String[] args) {
		
		// Test 1
		/*
		 * Afiche l'adresse memoire de l'objet (il y a une conversion syso(p) -> syso(p.toString()) )
		 */
		Personne p = new Personne();
		System.out.println(p);
		
		// Test 2
		/*
		 * Affiche les infos de p
		 */
		System.out.println();
		p.afficherInfos();
		
		// Test 3
		/*
		 * Affiche les infos de p apres modification directe sur les attributs
		 */
		System.out.println();
		// p.nom = "MASSAMBA";
		// p.prenom = "Astelia";
		p.age = 25;
		p.afficherInfos();
		
		// Test 4
		/*
		 * Affiche les infos de p2 lorsque l'on utilise le constructeur
		 */
		System.out.println();
		Personne p2 = new Personne("ZENINA", "Alexandra", 29);
		p2.afficherInfos();
		
		// Test 5
		/*
		 * Affiche les infos de p2 apres creation de la fonction .toString
		 */
		System.out.println(p2);
		
	}

}
