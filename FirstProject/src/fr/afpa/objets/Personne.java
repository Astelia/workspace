package fr.afpa.objets;

public class Personne {

	private String nom;
	protected String prenom;
	public int age;
	
	public Personne(String a, String b, int c) {
		nom = a;
		prenom = b;
		age = c;
	}
	
	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + ", age=" + age + "]";
	}

	public Personne() {
		// TODO Auto-generated constructor stub
	}

	public void afficherInfos() {
		System.out.println("Nom : "+nom);
		System.out.println("Prenom : "+prenom );
		System.out.println("Age : "+age);
	}
}
