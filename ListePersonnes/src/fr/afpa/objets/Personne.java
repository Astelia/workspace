package fr.afpa.objets;

public class Personne implements Comparable {

	private String nom;
	private String prenom;
	private String mail;
	
	
	public Personne(String nom, String prenom, String mail) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
	}

	
	
	public void affiche() {
		// Ne pas montrer l'id, mais pour voir ce que cela donne je le mets.
		System.out.println("nom = "+nom+", prenom = "+prenom+", mail = "+mail);
	}
	
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	
	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + ", mail=" + mail +"]";
	}


	@Override
	public int compareTo(Object o) {
		return this.nom.compareTo(((Personne) o).nom);
	}


	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Personne) {
			boolean nom = this.nom.equals(((Personne) obj).nom);
			boolean prenom = this.prenom.equals(((Personne) obj).prenom);
			boolean mail = this.mail.equals(((Personne) obj).mail);
			return nom && prenom && mail;
		}
		else {
			return false;
		}
		
	}
	
	
	
}
