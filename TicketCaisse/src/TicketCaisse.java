import java.util.Scanner;

public class TicketCaisse {

	static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		

		String code[]= {"0015154300","0016078185","0016064546","0016058844","0016084193"};
		float prix[]= {1.67f,1.99f,1.5f,599.99f,49.99f};	
		int compt[]= {0,0,0,0,0};

		boolean continuer = true;
		
		
		
		while (continuer) {
			System.out.print("Voulez-vous scanner un article ? (oui ou non) : ");
			String mot = scanner.next();
			scanner.nextLine();
			
			if (mot.equals("non")) {
				continuer = false;
			}
			else {
				saisieArticle(code, compt);
			}
		}
		
		scanner.close();
		
		System.out.println();
		afficherFixe();
		afficheLigneProduit(compt, prix);
		afficherTotal(compt, prix);
		afficherTotalArticles(compt);
	}
	
	
	
	
	public static void saisieArticle(String[] tableau, int[] cpt) {
		
		
		System.out.print("Entrez le code produit : ");
		String code = scanner.next();
		scanner.nextLine();
		
		while (!codeExiste(code, tableau, cpt)) {
			System.out.println("Code invalide !");
			System.out.print("Entrez un nouveau code : ");
			code = scanner.next();
			scanner.nextLine();
		}
	}
	
	
	
	public static boolean codeExiste(String code, String[] tableau, int[] cpt) {
		int i=0;
		while (i<tableau.length && !code.equals(tableau[i])) {
			i++;
		}
		if (i == tableau.length) {
			return false;
		}
		else {
			int[] temp = compt(code,cpt);
			return true;
		}
	}
	
	public static void afficheCodeExiste(String[] tab, int[] cpt) {
		boolean pasValide = codeExiste("truc", tab, cpt);
		System.out.println("Code non valide : "+pasValide);
		for (int i=0; i<cpt.length; i++) {
			System.out.println("Compteur "+i+" : "+cpt[i]);
		}
		
		System.out.println();
		boolean valide = codeExiste("0016064546", tab, cpt);
		System.out.println("Code valide : "+valide);
		for (int i=0; i<cpt.length; i++) {
			System.out.println("Compteur "+i+" : "+cpt[i]);
		}
		
	}
	
	
	static int[] compt(String code,int[]compt) {
		
		switch(code) {
			case "0015154300" : compt[0]+=1;break;
			case "0016078185" : compt[1]+=1;break;
			case "0016064546" : compt[2]+=1;break;
			case "0016058844" : compt[3]+=1;break;
			case "0016084193" : compt[4]+=1;break;
			default : ;
		
		}		
		return compt;
	}
	
	static void afficherChqCpt(String code,int[]compt) {
		int[] temp = compt(code,compt);
		for (int i=0; i<temp.length; i++) {
			System.out.println("Compteur "+i+" : "+temp[i]);
		}
	}
	
	static float[] totaux(float p[],int c[]) {
		
		float total[]=new float[p.length];

			for(int i=0;i<p.length;i++) {
				total[i]=p[i]*c[i]; 
			}
			return total;	
	}

	static float prixTotal(float t[]) {
		float totalT=0f;
		for(int i=0;i<t.length;i++) {
			totalT+=t[i];
		}
		return totalT;
	}

	static int comptTot(int c[]) {
		int comptT=0;
		for(int i=0;i<c.length;i++) {
			comptT+=c[i];
		}
		return comptT;
	}
	static float tva(float total) {
		return total*0.2f;
	}
	static void afficherTva(float prixT) {
		System.out.println("TVA: "+tva(prixT));
	}
	static void afficherCompT(int[] compt) {
		System.out.println(comptTot(compt)+" Articles");
	}
	static void afficherFixe() {
		System.out.println("						AFPA");
		System.out.println("					www.idp-JAVA_JEE.fr");
		System.out.println("					  03 00 00 00 00");
		System.out.println("Article               	     Quantit�       		 Prix");	
	}
	public static void afficheLigneProduit(int[] quantites, float[] prix) {
        for (int i=0; i<quantites.length; i++) {
            if (quantites[i] > 0) {
            	bidule :
                switch (i) {
                    case 0 : affichePainAuChocolat(quantites[i], prix[i]);
                                break bidule;
                    case 1 : afficheLait(quantites[i], prix[i]);
                                break bidule;
                    case 2 : afficheYaourt(quantites[i], prix[i]);
                                break bidule;
                    case 3 : afficheOrdinateur(quantites[i], prix[i]);
                                break bidule;
                    case 4 : afficheChaise(quantites[i], prix[i]);
                                break bidule;
                    default :;
                }
            }
        }
    }
    
    
    public static void affichePainAuChocolat(int quantite, float prix) {
        System.out.println("Pain au chocolat         	"+quantite+"                      	 "+prix);
    }
    
    public static void afficheLait(int quantite, float prix) {
        System.out.println("Lait                   		"+quantite+"                      	 "+prix);
    }
    
    public static void afficheYaourt(int quantite, float prix) {
        System.out.println("Yaourt                 		"+quantite+"                     	 "+prix);
    }
    
    public static void afficheOrdinateur(int quantite, float prix) {
        System.out.println("Ordinateur             		"+quantite+"                      	 "+prix);
    }
    
    public static void afficheChaise(int quantite, float prix) {
        System.out.println("Chaise                 		"+quantite+"                      	 "+prix);
    }
    public static void afficherTotal(int[] quantite, float []prix ) {
    	float total=0;
    	for(int i=0;i<quantite.length;i++) {
    		total+=quantite[i]*prix[i];
    	}
    	float tva=tva(total);
    	System.out.println("_________________________________________________________________");
		System.out.println("Total							 "+total);
		System.out.println("_________________________________________________________________");
		System.out.println("20%-TVA							 "+tva);
    }
    public static void afficherTotalArticles(int[]quantite) {
    	int total=0;
    	for(int i=0;i<quantite.length;i++) {
    		total+=quantite[i];
    	}
    	System.out.println("_________________________________________________________________");
		System.out.println("			    "+total+" Articles");	
		System.out.println("_________________________________________________________________");
		System.out.println();
		System.out.println("Date  : 19/11/2013    10:15");
    }
}