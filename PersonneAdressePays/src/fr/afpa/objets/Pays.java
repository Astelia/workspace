package fr.afpa.objets;

public class Pays {

	private String nom;
	private String codePays;
	
	
	public Pays(String nom, String codePays) {
		super();
		this.nom = nom;
		this.codePays = codePays;
	}

	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCodePays() {
		return codePays;
	}

	public void setCodePays(String codePays) {
		this.codePays = codePays;
	}
	
	@Override
	public String toString() {
		return "Pays [nom=" + nom + ", codePays=" + codePays + "]";
	}



}
