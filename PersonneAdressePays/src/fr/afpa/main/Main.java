package fr.afpa.main;

import fr.afpa.objets.Adresse;
import fr.afpa.objets.Pays;
import fr.afpa.objets.Personne;

public class Main {

	public static void main(String[] args) {
		
		Personne p1 = new Personne("Zimmer", "Marine", 20);
		Personne p2 = new Personne("Zenina", "Alexandra", 20);
		Personne p3 = new Personne("Gali", "Seti", 20);
		
		Pays france = new Pays("France", "10");
		Adresse adr1 = new Adresse("Pierre", "59000", "Lille", france, 24);
		Adresse adr2 = new Adresse("Paul", "59000", "Lille", france, 33);
		Adresse adr3 = new Adresse("Jacques", "59000", "Lille", france, 18);
		
		p1.getAdr()[0] = adr1;
		p1.getAdr()[1] = adr2;
		p1.getAdr()[2] = adr3;
		
		p2.getAdr()[0] = adr2;
		p2.getAdr()[1] = adr3;
		p2.getAdr()[2] = adr1;
		
		p3.getAdr()[0] = adr3;
		p3.getAdr()[1] = adr1;
		p3.getAdr()[2] = adr2;
		
		Personne[] tab = {p1, p2, p3};
		for (int i=0; i<3; i++) {
			System.out.println(tab[i]);
		}
	}

}
