# Projet Hotel V2

A fournir :

- Un fichier .csv avec la description des chambres dans cet ordre séparée par des ; et un type de chambre par ligne:
	* Le type de la chambre
	* Sa taille
	* Ses vues (séparées par des virgules)
	* Son occupation (2 adultes et 2 enfants,...)
	* Son tarif (un nombre réel positif)
	* Le nombre de chambres de ce type (un nombre entier positif)
	* Ses options (séparés par des |)

- Un fichier avec le mot de passe admin pour les options réserver une chambre, libérer une chambre, modifier une réservation et annuler une réservation. 

- Les logins des employés doivent tous commencer par GH avec 3 chiffres derrière (ex: "GH000"). Le login par défaut est GH000.

// Importer les .jar pour iText (PDF) et javax.mail.jar pour l'envoie de mail.