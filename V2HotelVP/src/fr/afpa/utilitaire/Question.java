package fr.afpa.utilitaire;

import java.time.LocalDate;
import java.util.Scanner;

public class Question {
	
	public static String scannerMail(Scanner scanner) {
		String mail = scanner.next();
		scanner.nextLine();
		while (!ControleSaisie.isStringMailValide(mail)) {
			System.out.println("Reponse invalide !");
			System.out.println("Veuillez entrer un mail valide (__@__.___) : ");
			mail = scanner.next();
			scanner.nextLine();
		}
		return mail;
	}
	
	/**
	 * Permet de recuperer un booleen entre dans la console.
	 * Affiche un message d'erreur et redemande une saisie si
	 * la reponse obtene n'est pas "oui" ou "non".
	 * @param scanner
	 * @return
	 */
	public static boolean scannerBoolean(Scanner scanner) {
		String booleen = scanner.next();
		scanner.nextLine();
		while (!ControleSaisie.isStringBoolean(booleen)) {
			System.out.println("Reponse invalide !");
			System.out.println("Veuillez repondre 'oui' ou 'non' : ");
			booleen = scanner.next();
			scanner.nextLine();
		}
		if (booleen.equalsIgnoreCase("oui")) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Permet de recuperer un nombre decimal entre dans la console.
	 * Affiche un message d'erreur et redemande une saisie si
	 * la reponse obtenue n'est pas un nombre decimal.
	 * @param scanner
	 * @return
	 */
	public static float scannerFloat(Scanner scanner) {
		String decimal = scanner.next();
		scanner.nextLine();
		while (!ControleSaisie.isStringFloat(decimal)) {
			System.out.println("Reponse invalide !");
			System.out.println("Veuillez entrer un nombre decimal "
					+ "(avec un '.' au lieu d'une ',') : ");
			decimal = scanner.next();
			scanner.nextLine();
		}
		return Float.parseFloat(decimal);
	}
	
	/**
	 * Permet de recuperer un entier entre dans la console.
	 * Affiche un message d'erreur et redemande une saisie si
	 * la reponse obtenue n'est pas un entier.
	 * @param scanner
	 * @return
	 */
	public static int scannerEntier(Scanner scanner) {
		String nombre = scanner.next();
		scanner.nextLine();
		while (!ControleSaisie.isStringEntier(nombre)) {
			System.out.println("Reponse invalide !");
			System.out.println("Veuillez entrer un entier : ");
			nombre = scanner.next();
			scanner.nextLine();
		}
		return Integer.parseInt(nombre);
	}
	
	/**
	 * Permet de recuperer une date entree dans la console.
	 * Affiche un message d'erreur et redemande une saisie si
	 * la reponse obtenue n'est pas une date ou une date valide.
	 * @param scanner
	 * @return
	 */
	public static LocalDate scannerDate(Scanner scanner) {
		String date = scanner.next();
		scanner.nextLine();
		while (!ControleSaisie.isStringDateValide(date)) {
			System.out.println("Reponse invalide !");
			System.out.println("Veuillez entrer une date valide "
					+ "(jj/mm/yyyy) : ");
			date = scanner.next();
			scanner.nextLine();
		}
		int[] res = ControleDate.conversionDateStringToTabInt(date);
		return LocalDate.of(res[2], res[1], res[0]);
	}
	
	/**
	 * Permet de recuperer une ligne entree dans la console.
	 * Le scanner est passe en parametre.
	 * @param scanner
	 * @return
	 */
	public static String scannerPhrase(Scanner scanner) {
		return scanner.nextLine();
	}
	
	/**
	 * Permet de recuperer une chaine de caractere jusqu'a un espace
	 * dans la console. Le scanner est passe en parametre.
	 * @param scanner
	 * @return
	 */
	public static String scannerMot(Scanner scanner) {
		String mot = scanner.next();
		scanner.nextLine();
		return mot;
	}
	
	/**
	 * Permet de recuperer un caractere passe en parametre dans
	 * la console. Le scanner est passe en parametre.
	 * @param scanner
	 * @return
	 */
	public static char scannerCharacter(Scanner scanner) {
		char lettre = scanner.next().charAt(0);
		scanner.nextLine();
		return lettre;
	}
	
}
