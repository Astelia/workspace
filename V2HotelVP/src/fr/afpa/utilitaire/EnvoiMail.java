package fr.afpa.utilitaire;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EnvoiMail {

	public static void envoyerMailSMTP(String cheminFichier,String destinataire) {
		
		String user = "batmantestjmail@gmail.com";
		String pass = "waynetech";
		String serveur = "smtp.gmail.com";
		
		try {
			// Etape 1 : Proprietes
			Properties props = System.getProperties();
			props.put("mail.smtp.host", serveur);
			props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.port", 587);
	        props.put("mail.smtp.auth", "true");
			Session session = Session.getInstance(props);
			
			// Etape 2 : Creation du message
			Message message = new MimeMessage(session);
			
			// Expediteur
			message.setFrom(new InternetAddress(user));
			
			// Destinataire(s)
			InternetAddress[] internetAddresses = new InternetAddress[1];
			internetAddresses[0] = new InternetAddress(destinataire);
			message.setRecipients(Message.RecipientType.TO,internetAddresses);
			
			// Objet du message
			message.setSubject("Reservation hotel");
			
			// Zone texte du mail
			MimeBodyPart body = new MimeBodyPart();
	        body.setText("Detail de votre reservation dans notre hotel en piece jointe");
	        
	        // Pièce jointe fichier
			MimeBodyPart attachMent1 = new MimeBodyPart();
			String fichier = cheminFichier;
	        attachMent1.attachFile(fichier);
	        
	        // Relie les parties entre elles
	        Multipart multipart = new MimeMultipart();
	        multipart.addBodyPart(body);
	        multipart.addBodyPart(attachMent1);
	        message.setContent(multipart);
			
	        // Permet d'envoyer le message
			Transport transport = session.getTransport("smtp");
			transport.connect(serveur, user, pass);
			transport.sendMessage(message, internetAddresses);
			transport.close();
			
		} catch (AddressException e) {
			System.out.println("L'epediteur n'existe pas !");
		} catch (IOException e) {
			System.out.println("Erreur d'entree/sortie");
		} catch (MessagingException e) {
			System.out.println("Probleme survenu lors de la construction du message.");
		}
		
		
        
	}
	
}
