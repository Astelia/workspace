package fr.afpa.objets;

public class TypeChambre {

	private int nbExemplaires;
	private String nom;
	private String occupation;
	private String[] listeVues;
	private String[] listeOptions;
	private int superficie;
	private float tarif;
	
	
	public TypeChambre(int nb, String nom_, String occupation_, String[] vues, String[] options, int superficie_, float tarif_) {
		nbExemplaires = nb;
		nom = nom_;
		occupation = occupation_;
		listeVues = vues;
		listeOptions = options;
		superficie = superficie_;
		tarif = tarif_;
	}

	/**
	 * Retourne une chaine de caracteres avec les infos sur
	 * le type d'une chambre.
	 * @return
	 */
	public String[] infosTypeChambre() {
		String[] res = new String[4+listeVues.length+listeOptions.length];
		res[0] = "Type de chambre : "+nom;
		res[1] = "Superficie : "+superficie;
		res[2] = "Occupation : "+occupation;
		res[3] = "Vues : \n";
		for (int i=0; i<listeVues.length; i++) {
			res[i+3] += (i+1)+") "+listeVues[i];
		}
		res[4+listeVues.length] = "Options : \n";
		for (int i=0; i<listeOptions.length; i++) {
			res[i+4+listeVues.length] += (i+1)+") "+listeOptions[i];
		}
		res[res.length-1] = "Tarif : "+tarif;
		return res;
	}
	
	/**
	 * Affichage des infos sur le type d'une chambre.
	 */
	public void infos() {
		for (int i=0; i<infosTypeChambre().length; i++) {
			System.out.println(infosTypeChambre()[i]);
		}
	}

	public int getNbExemplaires() {
		return nbExemplaires;
	}

	public void setNbExemplaires(int nbExemplaires_) {
		nbExemplaires = nbExemplaires_;
	}

	public String getNom() {
		return nom;
	}


	public void setNom(String nom_) {
		nom = nom_;
	}


	public String getOccupation() {
		return occupation;
	}


	public void setOccupation(String occupation_) {
		occupation = occupation_;
	}


	public String[] getVue() {
		return listeVues;
	}


	public void setVue(String[] vues) {
		listeVues = vues;
	}


	public String[] getListeOptions() {
		return listeOptions;
	}


	public void setListeOptions(String[] options) {
		listeOptions = options;
	}


	public int getSuperficie() {
		return superficie;
	}


	public void setSuperficie(int superficie_) {
		superficie = superficie_;
	}


	public float getTarif() {
		return tarif;
	}


	public void setTarif(float tarif_) {
		tarif = tarif_;
	}
	
	
	
	
}
