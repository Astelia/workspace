package fr.afpa.objets;

import java.time.LocalDate;
import java.util.Scanner;

import fr.afpa.objet.Client;
import fr.afpa.utilitaire.ControleSaisie;
import fr.afpa.utilitaire.LectureFichier;
import fr.afpa.utilitaire.Question;

public class Hotel {

	private String nom;
	private LocalDate dateDuJour;
	private Chambre[] chambres;
	private String[] employes;
	private Scanner in;
	
	
	public Hotel(String nom_, Scanner scanner) {
		nom = nom_;
		dateDuJour = LocalDate.now();
		chambres = creationHotel();
		employes = new String[1];
		employes[0] = "GH000";
		in = scanner;
		
		
	}

	
	/**
	 * Affichage du menu de l'hotel
	 */
	public void affichageMenuHotel() {
		System.out.println("-------------------  MENU HOTEL CDA JAVA -----------------------");
		System.out.println();
		System.out.println("A- Afficher l'etat de l'hotel");
		System.out.println("B- Afficher le nombre de chambre reservees");
		System.out.println("C- Afficher le nombre de chambre libres");
		System.out.println("D- Afficher le numero de la premiere chambre vide");
		System.out.println("E- Afficher le numero de la derniere chambre vide");
		System.out.println("F- Reserver une chambre");
		System.out.println("G- Liberer une chambre");
		System.out.println("H- Modifier une reservation");
		System.out.println("I- Annuler une reservation");
		System.out.println();
		System.out.println("Q- Quitter");
		System.out.println();
		System.out.println("-----------------------------------------------------------------");
		System.out.println();
		System.out.print("Votre choix : ");
	}
	
	/**
	 * Menu de l'hotel qui appelle les fonctions associees
	 */
	public void menuHotel() {
		affichageMenuHotel();
		String reponse = Question.scannerMot(in);
		switch (reponse) {
			case "A" : affichageChambres(listeChambresReservees()
							, "Liste des chambres reservees :");
					   break;
			case "B" : affichageNbChambresReservees();
					   break;
			case "C" : affichageNbChambresLibres();
					   break;
			case "D" : affichagePremiereChambre(true);
					   break;
			case "E" : affichageDerniereChambre(true);
					   break;
			case "F" : reserverChambre();
					   break;
			case "G" : // A completer
				   	   break;
			case "H" : // A completer
				   	   break;
			case "I" : annulerReservation();
				   	   break;
			default : System.out.println("Je ne comprends votre reponse.");
		}
		quitter(reponse);
	}
	
	/**
	 * Appelle la fonction menuHotel si la reponse n'est pas "Q"
	 * et met fin au programme sinon.
	 * @param reponse
	 */
	public void quitter(String reponse) {
		if (!reponse.equals("Q")) {
			menuHotel();
		}
	}
	
	// Mot de passe code en dur ? Non dans un fichier a lire donc. A modifier.
	/**
	 * Verifie si le mot de passe entre dans la console est correct.
	 * @return
	 */
	public boolean verifMotDePasse() {
		System.out.println("Veuillez entrer le mot de passe : ");
		String mdp = Question.scannerMot(in);
		return mdp.equals("bidule");
	}
	
	/**
	 * Affichage menu authentification
	 */
	public static void affichageMenuAuthentif() {
		System.out.println("_________________________________________________");
		System.out.println();
		System.out.println("             Entrer votre login                  ");
		System.out.println();
		System.out.println("_________________________________________________");
	}
	
	/**
	 * Permet a l'utilisateur de s'authentifier.
	 * Affiche le menu en boucle si login non valide ou si
	 * le client ou l'employe n'existent pas.
	 */
	public void menuAuthentif() {
		affichageMenuAuthentif();
		String login = Question.scannerMot(in);
		if (ControleSaisie.isStringLoginClientValide(login)
				&& clientExistParLogin(login)) {
			trouverClientParLogin(login).infos(chambres);
		}
		else if (ControleSaisie.isStringLoginEmployeValide(login)
				&& employeExistParLogin(login)) {
			menuHotel();
		}
		else {
			System.out.println("Login incorrect !");
			menuAuthentif();
		}
	}
	
	/**
	 * Verifie si l'employe existe dans le tableau des employes
	 * @param login
	 * @return
	 */
	public boolean employeExistParLogin(String login) {
		int i=0;
		while (i<employes.length && !employes[i].contentEquals(login)) {
			i++;
		}
		return i != employes.length;
	}
	
	/**
	 * ajoute un employe dans le tableau des employes
	 * s'il n'existe pas.
	 * @param login
	 */
	public void ajouterEmploye(String login) {
		if (!employeExistParLogin(login)) {
			nouveauTableauEmployes();
			employes[employes.length-1] = login;
		}
		else {
			System.out.println("L'employe existe deja !");
		}
	}
	
	/**
	 * Remplace l'ancien tableau d'employes avec un nouveau
	 * tableau d'employes avec les memes valeurs mais, une case en plus.
	 */
	private void nouveauTableauEmployes() {
		String[] res = new String[employes.length + 1];
		for (int i=0; i<employes.length; i++) {
			res[i] = employes[i];
		}
		employes = res;
	}
	
	/**
	 * Verifie si le client existe dans le tableau de clients
	 * @param login
	 * @return
	 */
	public boolean clientExistParLogin(String login) {
		return trouverClientParLogin(login) != null;
	}
	
	/**
	 * Retourne le client si celui-ci est dans le tableau
	 * des clients et null s'il ne le trouve pas a partir du login
	 * @param login
	 * @return
	 */
	public Client trouverClientParLogin(String login) {
		int i=0;
		boolean pasTrouve = true;
		while (i<chambres.length) {
			int j=0;
			
			while (j<chambres[i].getReservations().length && chambres[]) {
				
			}
			i++;
		}
		if (i == clients.length) {
			return null;
		}
		else {
			return clients[i];
		}
	}
	
	
	/**
	 * Retourne la premiere chambre qui est libre si bool = true 
	 * et occupee sinon
	 * S'il ne touve pas, il retourne -1
	 * @param bool
	 * @return
	 */
	public int premiereChambre(boolean bool) {
		int i=0;
		while (i<chambres.length && chambres[i].chambreLibre() != bool) {
			i++;
		}
		if (i == chambres.length) {
			return -1;
		}
		return i;
	}
	
	/**
	 * Affiche la premiere chambre qui est libre si bool = true 
	 * et occupee sinon et indique aussi s'il n'y en a pas.
	 * @param bool
	 */
	public void affichagePremiereChambre(boolean bool) {
		String mot = "";
		if (bool) {
			mot = "vide";
		}
		else {
			mot = "occupee";
		}
		int i = premiereChambre(bool);
		if (i>=0) {
			System.out.println("La premiere chambre "+mot
					+" est la numero "+(i+1));
		}
		else {
			System.out.println("Il n'y a pas de chambre "+mot+".");
		}
	}
	
	/**
	 * Retourne la derniere chambre qui est libre si bool = true et occupee sinon
	 * S'il ne touve pas, il retourne -1
	 * @param bool
	 * @return
	 */
	public int derniereChambre(boolean bool) {
		int i=chambres.length-1;
		while (i>=0 && chambres[i].chambreLibre() != bool) {
			i--;
		}
		if (i == -1) {
			return -1;
		}
		return i;
	}
	
	/**
	 * Affiche la derniere chambre qui est libre si bool = true 
	 * et occupee sinon et indique aussi s'il n'y en a pas.
	 * @param bool
	 */
	public void affichageDerniereChambre(boolean bool) {
		String mot = "";
		if (bool) {
			mot = "vide";
		}
		else {
			mot = "occupee";
		}
		int i = derniereChambre(bool);
		if (i>=0) {
			System.out.println("La derniere chambre "+mot
						+" est la numero "+(i+1));
		}
		else {
			System.out.println("Il n'y a pas de chambre "+mot+".");
		}
	}
	
	/**
	 * Affiche les infos d'une liste de chambres libres
	 * ou occupees passees en parametre.
	 * @param liste
	 * @param libre_reservees
	 */
	public void affichageChambres(Chambre[] liste, String libre_reservees) {
		System.out.println(libre_reservees);
		for (int i=0; i<liste.length; i++) {
			liste[i].getType().infos();
		}
	}
	
	/**
	 * Retourne la liste des chambres libres de l'hotel.
	 * @return
	 */
	public Chambre[] listeChambresLibres() {
		int nb = nbChambresLibres();
		Chambre[] listeChambres = new Chambre[nb];
		int i=0;
		int j=0;
		while (i<listeChambres.length && j<chambres.length) {
			if (chambres[j].chambreLibre()) {
				listeChambres[i] = chambres[j];
				i++;
			}
			j++;
		}
		return listeChambres;
	}
	
	/**
	 * Retourne la liste des chambres reservees de l'hotel.
	 * @return
	 */
	public Chambre[] listeChambresReservees() {
		int nb = nbChambresReservees();
		Chambre[] listeChambres = new Chambre[nb];
		int i=0;
		int j=0;
		while (i<listeChambres.length && j<chambres.length) {
			if (!chambres[j].chambreLibre()) {
				listeChambres[i] = chambres[j];
				i++;
			}
			j++;
		}
		return listeChambres;
	}
	
	/**
	 * Retourne le nombre de chambres  libres dans l'hotel.
	 * @return
	 */
	public int nbChambresLibres() {
		int res = 0;
		for (int i=0; i<chambres.length; i++) {
			if (chambres[i].chambreLibre()) {
				res++;
			}
		}
		return res;
	}
	
	/**
	 * Affiche le nombre de chambres libres dans l'hotel.
	 */
	public void affichageNbChambresLibres() {
		System.out.println("Il y a "+nbChambresLibres()+" chambres libres dans l'hotel.\n");
	}
	
	/**
	 * Retourne le nombre de chambres reservees dans l'hotel.
	 * @return
	 */
	public int nbChambresReservees() {
		int res = 0;
		for (int i=0; i<chambres.length; i++) {
			if (!chambres[i].chambreLibre()) {
				res++;
			}
		}
		return res;
	}
	
	/**
	 * Affiche le nombre de chambres reservees dans l'hotel.
	 */
	public void affichageNbChambresReservees() {
		System.out.println("Il y a "+nbChambresReservees()+" chambres reservees dans l'hotel.\n");
	}
	
	
	/**
	 * Cree la liste de chambres de l'hotel a partir des
	 * donnes contenues dans typeChambres.
	 * @return
	 */
	private Chambre[] creationHotel() {
		String fileName = "C:\\ENV\\Workspace\\HotelV2VProf\\hotelv2versionprof\\Fichiers_externes\\ListeChambres_V3.csv";
		TypeChambre[] types = LectureFichier.traductionFichier(fileName);
		int nb = nbTotalChambres(types);
		Chambre[] hotel = new Chambre[nb];
		int i=0;
		int j=0;
		while (i<types.length && j<nb) {
			for (int k=0; k<types[i].getNbExemplaires(); k++) {
				chambres[j] = new Chambre(types[i]);
				j++;
			}
			i++;
		}
		return hotel;
	}
	
	/**
	 * Calcul le nombre total de chambres a partir des
	 * donnes contenues dans types.
	 * @return
	 */
	public int nbTotalChambres(TypeChambre[] types) {
		int res = 0;
		for (int i=0; i<types.length; i++) {
			res += types[i].getNbExemplaires();
		}
		return res;
	}
	
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public LocalDate getDateDuJour() {
		return dateDuJour;
	}


	public void setDateDuJour(LocalDate date) {
		dateDuJour = date;
	}

	public Chambre[] getChambres() {
		return chambres;
	}

	public void setChambres(Chambre[] chambres) {
		this.chambres = chambres;
	}

	public String[] getEmployes() {
		return employes;
	}

	public void setEmployes(String[] employes) {
		this.employes = employes;
	}
	
	
	
}
