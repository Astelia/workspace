package fr.afpa.objets;

import java.time.LocalDate;

public class Reservation {

	private Client client;
	private LocalDate debut;
	private LocalDate fin;
	
	
	public Reservation(Client client_, LocalDate debut_, LocalDate fin_) {
		client = client_;
		debut = debut_;
		fin = fin_;
	}

	
	/**
	 * Calcule la duree du sejour a partir des dates de debut et de fin
	 * @return
	 */
	public int calculDureeSejour() {
		int i=0;
		LocalDate date = debut;
		while (!date.equals(fin)) {
			date = date.plusDays(1L);
			i++;
		}
		return i;
	}
	
	
	

	public Client getClient() {
		return client;
	}

	public void setClient(Client client_) {
		client = client_;
	}

	public LocalDate getDebut() {
		return debut;
	}

	public void setDebut(LocalDate debut_) {
		debut = debut_;
	}

	public LocalDate getFin() {
		return fin;
	}

	public void setFin(LocalDate fin_) {
		fin = fin_;
	}
	
	
	
	
}
