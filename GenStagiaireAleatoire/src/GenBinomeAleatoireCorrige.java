import java.util.Random;

public class GenBinomeAleatoireCorrige {

	public static void main(String[] args) {
		
		String listeStagiaires[] = {"Alexandra", "Nicolas","Guillaume","Stephane","Seti","Gaetan","Ali","Hakim", "Astelia", "Julien","David","Marine","Fayaz","Charles","Samuel"};
		int listeValeursGen[] = {-1, -1, -1, -1, -1, -1, -1};
		int indiceTetePool = 8;
		int indice;
		for (int i = 0; i < listeValeursGen.length; i++) {
			System.out.print(listeStagiaires[indiceTetePool]+" / ");
			indice = nouveauStagiaire(listeValeursGen);
			System.out.println(listeStagiaires[indice]);
			listeValeursGen[i] = indice;
			indiceTetePool++;
		}
		indice = nouveauStagiaire(listeValeursGen);
		System.out.println(listeStagiaires[indice]);
	}

	static int nouveauStagiaire(int[] listeValeursGen) {
		int indice = -1;
		boolean nouveau = false;
		while (!nouveau) {
			nouveau = true;
			indice = aleatoireInferieur(8);
			for (int i = 0; i < listeValeursGen.length; i++) {
				if (listeValeursGen[i] == indice) {
					nouveau = false;
				}
			}
		}
		return indice;
	}

	public static int aleatoireInferieur(int max) {
		return new Random().nextInt(max);
	}
}
