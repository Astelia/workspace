package main;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import objetsEpures.Email;

public class Main {

	public static void main(String[] args) throws AddressException, MessagingException, IOException {
		
		Email.envoyerMailSMTP(true);
	}
	
}
