package objetsEpures;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Email {

	public static void envoyerMailSMTP(boolean debug) throws AddressException, MessagingException, IOException {
		
		String user = "batmantestjmail@gmail.com";
		String pass = "waynetech";
		String serveur = "smtp.gmail.com"; // Utiliser : C:\> ping 74.125.140.109 ou ping smtp.gmail.com
		
		
		
		// Etape 1 : Proprietes
		Properties props = System.getProperties();
		props.put("mail.smtp.host", serveur);
		props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", 587);
        props.put("mail.smtp.auth", "true");
		
        
        // Etape 2 : Session
		Session session = Session.getInstance(props);
		
		// Etape 3 : Creation du message
		Message message = new MimeMessage(session);
		
		// Expéditeur
		message.setFrom(new InternetAddress(user));
		
		// Destinataire(s)
		InternetAddress[] internetAddresses = new InternetAddress[1];
		internetAddresses[0] = new InternetAddress(user);
		
		
		//Etape 4 : Structure message
		message.setRecipients(Message.RecipientType.TO,internetAddresses);
		message.setSubject("Test AFPA");
		//session.setDebug(debug);
		
		// Zone texte du mail
		MimeBodyPart body = new MimeBodyPart();
        body.setText("Merci pour le petit dej'.");
        
        // Piece jointe fichier
		MimeBodyPart attachMent1 = new MimeBodyPart();
		String fichier = "C:\\Users\\59013-70-05\\Desktop\\Pr�sentations\\pr�sentation QR code.odp";
        attachMent1.attachFile(fichier);
        
        // Piece jointe image
        MimeBodyPart imageJointe = new MimeBodyPart();
        String image = "C:\\ENV\\Annexes\\QRCode\\vCard.png";
        imageJointe.attachFile(image);
        
        // Relie les parties entre elles
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(body);
        multipart.addBodyPart(attachMent1);
        multipart.addBodyPart(imageJointe);
        message.setContent(multipart);
		
        // Permet d'envoyer le message
		Transport transport = session.getTransport("smtp");
		transport.connect(serveur, user, pass);
		transport.sendMessage(message, internetAddresses);
		transport.close();
        
	}
	
}
