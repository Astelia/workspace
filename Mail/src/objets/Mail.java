package objets;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Mail {
	
	//private final static String MAILER_VERSION = "Java";
	
	public static void envoyerMailSMTP(boolean debug) throws AddressException, MessagingException, IOException {
		
		String user = "batmantestjmail@gmail.com";
		String pass = "waynetech";
		String serveur = "smtp.gmail.com";
		//String serveur2 = "smtp.yahoo.com";
		
		// Etape 1 : Propriétés
		Properties props = System.getProperties();
		props.setProperty("mail.from", user);
		props.put("mail.smtp.host", serveur);
		props.put("mail.smtp.starttls.enable", "true");
        //props.put("mail.smtp.user", user);
        //props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", 587);
        props.put("mail.smtp.auth", "true");
		
        
        // Etape 2 : Session
		Session session = Session.getInstance(props);//Session.getInstance(props, new GMailAuthenticator(user, pass));//
		
		
		// Etape 3 : Creation du message
		Message message = new MimeMessage(session);
		
		// Expéditeur
		message.setFrom(new InternetAddress(user));
		
		// Destinataire(s)
		InternetAddress[] internetAddresses = new InternetAddress[1];
		internetAddresses[0] = new InternetAddress(user);
		
		// Structure message
		message.setRecipients(Message.RecipientType.TO,internetAddresses);
		message.setSubject("Test");
		//message.setHeader("X-Mailer", MAILER_VERSION);
		//message.setSentDate(new Date());
		session.setDebug(debug);
		
		// Corps du mail
		MimeBodyPart body = new MimeBodyPart();
        body.setText("Je suis la nuit. Je suis la justice. Je suis Batman !");
        
        // Pièce jointe
		MimeBodyPart attachMent = new MimeBodyPart();
		String fichier = "G:\\Info\\AFPA CDA 2019-2020\\Annexe\\QRCode\\Test.png";
        attachMent.attachFile(fichier);
        
        // Relie les parties entre elles
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(body);
        multipart.addBodyPart(attachMent);
        message.setContent(multipart);
		
        // Permet d'envoyer le message
		Transport transport = session.getTransport("smtp");
		transport.connect(serveur, user, pass);
		transport.sendMessage(message, internetAddresses);
		transport.close();
        
       
        //Transport.send(message);
	}
	
}
