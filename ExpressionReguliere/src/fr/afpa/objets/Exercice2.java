package fr.afpa.objets;

import java.util.regex.*;

public class Exercice2 {

	// Il va prendre un maximum de caracteres d'ou le resultat
	public static void exercice2() {
		String source = "yyxxxyxx";
		String pattern = ".*xx";
		
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(source);
		while (m.find()) {
			System.out.println(m.start()+" "+m.group());
		}
	}
	
}
