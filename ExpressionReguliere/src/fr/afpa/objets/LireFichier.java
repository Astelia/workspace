package fr.afpa.objets;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LireFichier {

	public static String lireFichier(String fileName) {
		String res = "";
		try {
			FileReader fr = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);
			while (br.ready()) {
				res += br.readLine();
			}
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("Le fichier n'existe pas.");
		} catch (IOException e) {
			System.out.println("Erreur entree/sortie.");
		}
		return res;
	}
	
}
