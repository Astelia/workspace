package fr.afpa.objets;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class EcrireFichier {

	public static void ecrireFichier(String texte) {
		try {
			FileWriter fw = new FileWriter("Fichiers_externes\\mailsValides.txt", true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(texte);
			bw.newLine();
			bw.close();
		}
		catch (IOException e) {
			System.out.println("Probleme d'entree sortie.");
		}
	}
	
}
