package fr.afpa.exercice6Objets;

import java.time.LocalDate;
import java.util.Scanner;

import fr.afpa.exercice6.utilitaire.Controle;

public class Saisie {
	
	/**
	 * Fonction qui permet de recuperer un numero de telephone entre dans la console.
	 * @param scanner
	 * @return
	 */
	public static String telephone(Scanner scanner) {
		String tel = scanner.next();
		scanner.nextLine();
		while (!Controle.telephone(tel)) {
			System.out.println("Numero de telephone invalide ! (10 chiffres attendus)");
			telephone(scanner);
		}
		return tel;
	}
	
	/**
	 * Fonction qui permet de recuperer un code postal entre dans la console.
	 * @param scanner
	 * @return
	 */
	public static String codePostal(Scanner scanner) {
		String code = scanner.next();
		scanner.nextLine();
		while (!Controle.codePostal(code)) {
			System.out.println("Code postal invalide ! (5 chiffres attendus)");
			codePostal(scanner);
		}
		return code;
	}
	
	/**
	 * Fonction qui permet de recuperer un code pays entre dans la console.
	 * @param scanner
	 * @return
	 */
	public static String codePays(Scanner scanner) {
		String code = scanner.next();
		scanner.nextLine();
		while (!Controle.codePays(code)) {
			System.out.println("Code pays invalide ! (2 caracteres alphanumeriques attendus)");
			codePays(scanner);
		}
		return code;
	}
	
	/**
	 * Fonction qui permet de recuperer un email entre dans la console.
	 * @param scanner
	 * @return
	 */
	public static String email(Scanner scanner) {
		String mail = scanner.next();
		scanner.nextLine();
		while (!Controle.email(mail)) {
			System.out.println("Email invalide !");
			email(scanner);
		}
		return mail;
	}
	
	/**
	 * Fonction qui permet de recuperer une date entree dans la console.
	 * @param scanner
	 * @return
	 */
	public static LocalDate date(Scanner scanner) {
		String date = scanner.next();
		scanner.nextLine();
		while (!Controle.date(date)) {
			System.out.println("Date invalide ! (format jj/mm/aaaa attendu)");
			date(scanner);
		}
		return creerDate(date);
	}
	
	/**
	 * Fonction qui permet de convertir un String en un LocalDate.
	 * @param date
	 * @return
	 */
	private static LocalDate creerDate(String date) {
		String[] dateTableau = date.split("/");
		return LocalDate.of(Integer.parseInt(dateTableau[2]), Integer.parseInt(dateTableau[1]), Integer.parseInt(dateTableau[0]));
	}
	/**
	 * Fonction qui permet de recuperer un entier entre dans la console.
	 * @param scanner
	 * @return
	 */
	
	public static int entier(Scanner scanner) {
		String nb = scanner.next();
		scanner.nextLine();
		while (!Controle.entier(nb)) {
			System.out.println("Ceci n'est pas un entier !");
			entier(scanner);
		}
		return Integer.parseInt(nb);
	}
	
	/**
	 * Fonction qui permet de recuperer un float entre dans la console.
	 * @param scanner
	 * @return
	 */
	public static float flottant(Scanner scanner) {
		String nb = scanner.next();
		scanner.nextLine();
		while (!Controle.flottant(nb)) {
			System.out.println("Ceci n'est pas un nombre decimal !");
			flottant(scanner);
		}
		return Float.parseFloat(nb);
	}
}
