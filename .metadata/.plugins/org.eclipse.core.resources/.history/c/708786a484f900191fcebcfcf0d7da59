package fr.afpa.exercice6.main;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import fr.afpa.exercice6.objets.Adresse;
import fr.afpa.exercice6.objets.Pays;
import fr.afpa.exercice6.objets.Personne;
import fr.afpa.exercice6.utilitaire.Saisie;

public class Main {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Creation de la liste de personnes.");
		List<Personne> listePersonnes = creerPersonnes(scanner);
		for (Personne personne : listePersonnes) {
			System.out.println(personne);
		}
		System.out.println();
		
		System.out.println("Affichage des N premieres personnes de la liste.");
		afficher(5, listePersonnes);
		System.out.println();
		
		// Affichage de toutes les personnes nees apres une date donnee
		naissanceApres(LocalDate.of(1990, 10, 02), listePersonnes);
		System.out.println();
		
		// Affichage de toutes les personnes de la liste triees par nom
		trierListePersonnes(scanner, listePersonnes);
		for (Personne personne : listePersonnes) {
			System.out.println(personne);
		}
		System.out.println();
		
		// Affichage de toutes les personnes de la liste triees par penom
		trierListePersonnes(scanner, listePersonnes);
		for (Personne personne : listePersonnes) {
			System.out.println(personne);
		}
		System.out.println();
		
		// Affichage de toutes les personnes de la liste triees par distance
		trierListePersonnes(scanner, listePersonnes);
		for (Personne personne : listePersonnes) {
			System.out.println(personne);
		}
		System.out.println();
		
		// Recherche d'une personne par son nom
		rechercher("nom", "Grayson", listePersonnes);
		System.out.println();
		
		// Recherche d'une personne par son prenom
		rechercher("prenom", "Damian", listePersonnes);
		System.out.println();
		
		scanner.close();
	}
	
	
	/**
	 * Permet de choisir de quelle maniere la liste de personnes est triee.
	 * @param scanner
	 * @return
	 */
	private static String choixTri(Scanner scanner) {
		System.out.println("Comment trier le tableau (par nom, par prenom ou par distance) ? :");
		String mot = Saisie.mot(scanner);
		while (!"nom".equalsIgnoreCase(mot) || !"prenom".equalsIgnoreCase(mot) || !"distance".equalsIgnoreCase(mot)) {
			System.out.println("Choix non valide ! 'nom', 'prenom' ou 'distance' attendu.");
			choixTri(scanner);
		}
		return mot;
	}
	
	/**
	 * Permet de trier une liste de personnes par nom, par prenom, ou par distance avec le centre de formation.
	 * @param scanner
	 * @param listePersonnes
	 */
	public static void trierListePersonnes(Scanner scanner, List<Personne> listePersonnes) {
		Personne.setTri(choixTri(scanner));
		Collections.sort(listePersonnes);
	}
	
	/**
	 * Permet d'afficher les informations de la personne trouvee dans listePersonnes a partir de son nom et de son prenom.
	 * Si la personne n'est pas trouvee, affiche le message <<Personne non trouvee !>>
	 * @param nom
	 * @param prenom
	 * @param listePersonnes
	 */
	public static void rechercher(String critere, String valeur, List<Personne> listePersonnes) {
		if ("nom".equalsIgnoreCase(critere)) {
			rechercheParNom(valeur, listePersonnes);
		}
		else if ("prenom".equalsIgnoreCase(critere)) {
			rechercheParPrenom(valeur, listePersonnes);
		}
		else {
			System.out.println("Il faut choisir entre 'nom' et 'prenom' pour critere !");
		}
	}
	
	/**
	 * Recherche d'une personne dans la liste par son nom.
	 * @param nom
	 * @param listePersonnes
	 */
	private static void rechercheParNom(String nom, List<Personne> listePersonnes) {
		int i = 0;
		while (i<listePersonnes.size() && !listePersonnes.get(i).getNom().equals(nom)) {
			i++;
		}
		if (i<listePersonnes.size()) {
			System.out.println(listePersonnes.get(i));
		}
		else {
			System.out.println("Personne non trouvee !");
		}
	}
	
	/**
	 * Recherche d'une personne dans la liste par son prenom.
	 * @param prenom
	 * @param listePersonnes
	 */
	private static void rechercheParPrenom(String prenom, List<Personne> listePersonnes) {
		int i = 0;
		while (i<listePersonnes.size() && !listePersonnes.get(i).getPrenom().equals(prenom)) {
			i++;
		}
		if (i<listePersonnes.size()) {
			System.out.println(listePersonnes.get(i));
		}
		else {
			System.out.println("Personne non trouvee !");
		}
	}
	
	
	/**
	 * Affiche toutes les personnes nees apres la date passee en parametre.
	 * @param date
	 * @param listePersonnes
	 */
	public static void naissanceApres(LocalDate date, List<Personne> listePersonnes) {
		for (Personne personne : listePersonnes) {
			if (personne.getDateNaissance().isAfter(date)) {
				System.out.println(personne);
			}
		}
	}
	
	/**
	 * Affiche les "nb" premieres personnes de la liste.
	 * @param nb
	 * @param listePersonnes
	 */
	public static void afficher(int nb, List<Personne> listePersonnes) {
		for (int i=0; i<listePersonnes.size() && i<nb; i++) {
			System.out.println(listePersonnes.get(i));
		}
	}
	
	/**
	 * Retourne une liste de personnes.
	 * @param scanner
	 * @return
	 */
	public static List<Personne> creerPersonnes(Scanner scanner) {
		Adresse[] adresses = creationTabAdresses();
		
		List<Personne> listePersonnes = new ArrayList<Personne>();
		listePersonnes.add(creationPersonne(scanner));
		listePersonnes.add(new Personne("Grayson", "Richard", LocalDate.of(1995, 03, 25), "fre.hytgr@5glkh.fr", "0123456789", adresses[1]));
		listePersonnes.add(new Personne("Drake", "Tim", LocalDate.of(1998, 02, 18), "fpe.hyt9r@5glkh.fr", "0123456789", adresses[6]));
		listePersonnes.add(new Personne("Gordon", "Barbara", LocalDate.of(1995, 04, 22), "gre.hhtgr@5g5kh.fr", "0123456789", adresses[7]));
		listePersonnes.add(new Personne("Wayne", "Damian", LocalDate.of(1999, 03, 25), "frg.hyhgr@5gkkh.fr", "0123456789", adresses[3]));
		listePersonnes.add(new Personne("Todd", "Jason", LocalDate.of(1996, 12, 29), "fke.hytnr@5gmkh.fr", "0123456789", adresses[4]));
		listePersonnes.add(new Personne("Dent", "Harvey", LocalDate.of(1980, 05, 25), "abc.hyhgr@5g6kh.fr", "0123456789", adresses[2]));
		listePersonnes.add(new Personne("Norton", "Edward", LocalDate.of(1986, 01, 14), "fre.hytgr@5glkh.fr", "0123456789", adresses[9]));
		listePersonnes.add(new Personne("Cobblepot", "Oswald", LocalDate.of(1978, 11, 01), "fce.hynbr@5gl56h.fr", "0123456789", adresses[5]));
		listePersonnes.add(new Personne("Quinzel", "Harleen", LocalDate.of(1984, 02, 24), "are.hztgr@5glkh.fr", "0123456789", adresses[8]));
		listePersonnes.add(new Personne("Joker", "Jack", LocalDate.of(1983, 02, 23), "frt.hopgr@5glkh.fr", "0123456789", adresses[0]));
		return listePersonnes;
	}
	
	/**
	 * Retourne un tableau d'adresses
	 * @return
	 */
	private static Adresse[] creationTabAdresses() {
		Pays pays = new Pays("France", "FR");
		
		Adresse[] res = new Adresse[10];
		res[0] = new Adresse("1", "truc", "59000", "Lille", pays, 51.5f);
		res[1] = new Adresse("2", "bidule", "59000", "Lille", pays, 52.5f);
		res[2] = new Adresse("3", "machin", "59000", "Lille", pays, 53.5f);
		res[3] = new Adresse("4", "chouette", "59000", "Lille", pays, 54.5f);
		res[4] = new Adresse("5", "muche", "59000", "Lille", pays, 55.5f);
		res[5] = new Adresse("6", "truc", "59000", "Lille", pays, 56.5f);
		res[6] = new Adresse("7", "bidule", "59000", "Lille", pays, 57.5f);
		res[7] = new Adresse("8", "machin", "59000", "Lille", pays, 58.5f);
		res[8] = new Adresse("9", "chouette", "59000", "Lille", pays, 59.5f);
		res[9] = new Adresse("10", "muche", "59000", "Lille", pays, 50.5f);
		return res;
	}
	
	
	/**
	 * Retourne une personne cree a partir de donnees entrees dans la console.
	 * @param scanner
	 * @return
	 */
	private static Personne creationPersonne(Scanner scanner) {
		System.out.print("Entrer nom : ");
		String nom = Saisie.mot(scanner);
		
		System.out.print("Entrer prenom : ");
		String prenom = Saisie.mot(scanner);
		
		System.out.print("Entrer date de naissance : ");
		LocalDate date = Saisie.date(scanner);
		
		System.out.print("Entrer email "
				+ "([3 carateres alphabetiques].[de 5 a 12 caracteres alphanumerique]@[de 4 a 8 caracteres alphanumerique].[de 2 a 3 caracteres alphabetique]) : ");
		String email = Saisie.email(scanner);
		
		System.out.print("Entrer numero de telephone : ");
		String tel = Saisie.telephone(scanner);
		
		System.out.print("Entrer distance entre le domicile et le centre de formation : ");
		float distance = Saisie.flottant(scanner);
		
		System.out.print("Entrer numero de la rue : ");
		String numRue = Saisie.mot(scanner);
		
		System.out.print("Entrer libelle de la rue : ");
		String libelleRue = Saisie.mot(scanner);
		
		System.out.print("Entrer code postal : ");
		String codePostal = Saisie.codePostal(scanner);
		
		System.out.print("Entrer ville : ");
		String ville = Saisie.mot(scanner);
		
		System.out.print("Entrer nom du pays : ");
		String nomPays = Saisie.mot(scanner);
		
		System.out.print("Entrer code pays : ");
		String codePays = Saisie.codePays(scanner);
		
		Pays pays = new Pays(nomPays, codePays);
		Adresse adresse = new Adresse(numRue, libelleRue, codePostal, ville, pays, distance);
		return new Personne(nom, prenom, date, email, tel, adresse);
	}
	

}
