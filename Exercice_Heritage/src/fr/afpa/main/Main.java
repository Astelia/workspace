package fr.afpa.main;

import fr.afpa.objets.Personne;
import fr.afpa.objets.Profil;
import fr.afpa.objets.Utilisateur;

public class Main {

	public static void main(String[] args) {
		
		// Question 3
		Profil cp = new Profil("CP", "Chef de projet");
		Profil mn = new Profil("MN", "Manager");
		Profil dp = new Profil("DP", "Directeur de projet");
		Profil drh = new Profil("DRH", "Directeur des ressources humaines");
		Profil dg = new Profil("DG", "Directeur general");
		//Profil[] tabProfils = {cp, mn, dp, drh, dg};
		
		// Question 4
		Personne[] tabPersonnes = new Utilisateur[6];
		tabPersonnes[0] = new Utilisateur("Wayne", "Bruce", "batman@gmail.com", "0123456789", 1000000d, "batman", "waynetech", "Gotham City", dg);
		tabPersonnes[1] = new Utilisateur("Grayson", "Richard", "nightwing@gmail.com", "1234567890", 5000d, "nightwing", "baton", "Bloodhaven", drh);
		tabPersonnes[2] = new Utilisateur("Wayne", "Damian", "robin@gmail.com", "2345678901", 2500d, "robin", "katana", "Gotham City", cp);
		tabPersonnes[3] = new Utilisateur("Todd", "Jason", "arsenal@gmail.com", "3456789012", 2000d, "arsenal", "bazooka", "Gotham", mn);
		tabPersonnes[4] = new Utilisateur("Gordon", "Barbara", "batgirl@gmail.com", "4567890123", 3000d, "batgirl", "batarang", "Gotham City", dp);
		tabPersonnes[5] = new Utilisateur("Drake", "Tim", "robin3@gmail.com", "5678901234", 2100d, "robin3", "bouclier", "Gotham City", mn);

		// Question 5
		for (int i=0; i<tabPersonnes.length; i++) {
			tabPersonnes[i].affiche();
			System.out.println();
		}
		System.out.println("\n");
		
		// Question 6
		for (int i=0; i<tabPersonnes.length; i++) {
			if (tabPersonnes[i] instanceof Utilisateur && "MN".equals(((Utilisateur) tabPersonnes[i]).getProfil().getCode())) {
				tabPersonnes[i].affiche();
				System.out.println();
			}
		}
		System.out.println("\n");
	}

}
