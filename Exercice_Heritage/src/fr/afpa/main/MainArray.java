package fr.afpa.main;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

import fr.afpa.objets.Personne;
import fr.afpa.objets.Profil;
import fr.afpa.objets.Utilisateur;

public class MainArray {

	public static void main(String[] args) {
		
		// Question 3
		Profil cp = new Profil("CP", "Chef de projet");
		Profil mn = new Profil("MN", "Manager");
		Profil dp = new Profil("DP", "Directeur de projet");
		Profil drh = new Profil("DRH", "Directeur des ressources humaines");
		Profil dg = new Profil("DG", "Directeur general");
		
		// Question 4
		Personne bruce = new Utilisateur("Wayne", "Bruce", "batman@gmail.com", "0123456789", 1000000d, "batman", "waynetech", "Gotham City", dg);
		Personne richard = new Utilisateur("Grayson", "Richard", "nightwing@gmail.com", "1234567890", 5000d, "nightwing", "baton", "Bloodhaven", drh);
		Personne damian = new Utilisateur("Wayne", "Damian", "robin@gmail.com", "2345678901", 2500d, "robin", "katana", "Gotham City", cp);
		Personne jason = new Utilisateur("Todd", "Jason", "arsenal@gmail.com", "3456789012", 2000d, "arsenal", "bazooka", "Gotham", mn);
		Personne barbara = new Utilisateur("Gordon", "Barbara", "batgirl@gmail.com", "4567890123", 3000d, "batgirl", "batarang", "Gotham City", dp);
		Personne tim = new Utilisateur("Drake", "Tim", "robin3@gmail.com", "5678901234", 2100d, "robin3", "bouclier", "Gotham City", mn);

		List<Personne> listePersonnes = new ArrayList<Personne>();
		listePersonnes.add(bruce);
		listePersonnes.add(richard);
		listePersonnes.add(damian);
		listePersonnes.add(jason);
		listePersonnes.add(barbara);
		listePersonnes.add(tim);
		
		Collections.sort(listePersonnes);
		for (Personne personne : listePersonnes) {
			System.out.println(personne+" salaire :"+personne.calculerSalaire());
		}
		System.out.println();
		
		System.out.println("La liste de personnes contient Jason ? : "+listePersonnes.contains(jason));
		
	}

}
