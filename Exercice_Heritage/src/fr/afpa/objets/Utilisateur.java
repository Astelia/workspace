package fr.afpa.objets;

public class Utilisateur extends Personne {

	private String login;
	private String password;
	private String service;
	private Profil profil;
	
	
	public Utilisateur(String nom, String prenom, String mail, String telephone, double salaire, String login,
			String password, String service, Profil profil) {
		super(nom, prenom, mail, telephone, salaire);
		this.login = login;
		this.password = password;
		this.service = service;
		this.profil = profil;
	}

	
	@Override
	public double calculerSalaire() {
		if ("MN".equals(this.profil.getCode())) {
			return super.getSalaire() + super.getSalaire()*10/100;
		}
		else if ("DG".equals(this.profil.getCode())) {
			return super.getSalaire() + super.getSalaire()*40/100;
		}
		else {
			return super.calculerSalaire();
		}
	}

	@Override
	public void affiche() {
		super.affiche();
		System.out.println("login = "+login+", password = "+password+", service = "+service+", profil = " + profil);
	}


	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public Profil getProfil() {
		return profil;
	}

	public void setProfil(Profil profil) {
		this.profil = profil;
	}


	@Override
	public String toString() {
		return "Utilisateur [login=" + login + ", password=" + password + ", service=" + service + ", profil=" + profil
				+ "]";
	}


	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Utilisateur) {
			boolean personne = super.equals(obj);
			boolean login = this.login.equals(((Utilisateur) obj).login);
			boolean password = this.password.equals(((Utilisateur) obj).password);
			boolean service = this.service.equals(((Utilisateur) obj).service);
			boolean profil = this.profil.equals(((Utilisateur) obj).profil);
			return personne && login && password && service && profil;
		}
		else {
			return false;
		}

	}
	
	
	
}
