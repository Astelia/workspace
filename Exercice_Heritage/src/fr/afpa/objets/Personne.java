package fr.afpa.objets;

public class Personne implements Comparable {

	private static int cpt = 0;
	
	private int id;
	private String nom;
	private String prenom;
	private String mail;
	private String telephone;
	private double salaire;
	
	
	public Personne(String nom, String prenom, String mail, String telephone, double salaire) {
		super();
		this.id = ++cpt;
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.telephone = telephone;
		this.salaire = salaire;
	}

	
	public double calculerSalaire() {
		return salaire ;
	}
	
	public void affiche() {
		// Ne pas montrer l'id, mais pour voir ce que cela donne je le mets.
		System.out.println("id = "+id+", nom = "+nom+", prenom = "+prenom+", mail = "+mail+", telephone = "+telephone+", salaire = "+calculerSalaire());
	}
	
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public double getSalaire() {
		return salaire;
	}

	public void setSalaire(double salaire) {
		this.salaire = salaire;
	}


	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", mail=" + mail + ", telephone="
				+ telephone + ", salaire=" + salaire + "]";
	}


	@Override
	public int compareTo(Object o) {
		if (this.calculerSalaire() == ((Personne) o).calculerSalaire()) {
			return 0;
		}
		else if (this.calculerSalaire() < ((Personne) o).calculerSalaire()) {
			return -1;
		}
		else {
			return 1;
		}
	}


	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Personne) {
			boolean id =  this.id == ((Personne) obj).id;
			boolean nom = this.nom.equals(((Personne) obj).nom);
			boolean prenom = this.prenom.equals(((Personne) obj).prenom);
			boolean mail = this.mail.equals(((Personne) obj).mail);
			boolean telephone = this.telephone.equals(((Personne) obj).telephone);
			boolean salaire = this.salaire == ((Personne) obj).salaire;
			return id && nom && prenom && mail && telephone && salaire;
		}
		else {
			return false;
		}
		
	}
	
	
	
}
