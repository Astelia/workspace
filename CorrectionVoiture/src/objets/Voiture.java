package objets;

public class Voiture {

	private String couleur;
	private String plaqueImmat;
	private int vitesse;
	private int vitesseMax;
	private boolean etatMoteur;
	
	
	public Voiture(String a, String b, int c, int d, boolean e) {
		couleur = a;
		plaqueImmat = b;
		vitesse = c;
		vitesseMax = d;
		etatMoteur = e;
	}
	
	
	public void demarrer() {
		if (isEtatMoteur() == true) {
			System.out.println("Le moteur est demarre !");
		}
		else {
			setEtatMoteur(true);
		}
	}
	
	/**
	 * 
	 * @param acceleration
	 */
	public void accelerer(int acceleration) {
		if (isEtatMoteur()) {
			if (getVitesse()+acceleration >= getVitesseMax()) {
				setVitesse(getVitesseMax());
				System.out.println("La voiture "+couleur+" est a : "+getVitesse());
			}
			else {
				setVitesse(getVitesse() + acceleration);
				System.out.println("La voiture "+couleur+" est a : "+getVitesse());
			}
		}
	}
	
	public void freiner(int freinage) {
		if (isEtatMoteur() && getVitesse()>=freinage) {
			setVitesse(getVitesse() - freinage);
			System.out.println("La voiture "+couleur+" est a : "+getVitesse());
		}
		else {
			setVitesse(0);
		}
	}

	public boolean isEtatMoteur() {
		return etatMoteur;
	}
	
	public void setEtatMoteur(boolean eM) {
		etatMoteur = eM;
	}
	
	public int getVitesseMax() {
		return vitesseMax;
	}


	public void setVitesseMax(int vM) {
		vitesseMax = vM;
	}
	
	public int getVitesse() {
		return vitesse;
	}


	public void setVitesse(int v) {
		vitesse = v;
	}
}
