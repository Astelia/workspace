package objets;

public class Radar {

	private int vitesseLimit;
	
	
	public Radar(int v) {
		vitesseLimit = v;
	}
	
	
	public int getVitesseLimit() {
		return vitesseLimit;
	}


	public void setVitesseLimit(int vL) {
		vitesseLimit = vL;
	}


	public boolean scan(Voiture voiture) {
		if (voiture.getVitesse() > getVitesseLimit()) {
			int amende = voiture.getVitesse() - getVitesseLimit();
			if (amende <= 5) {
				System.out.println("-2 pts et 100 000 euros d'amende");
			}
			else if (amende>5 && amende<=10) {
				System.out.println("-1 pt et 2 euros d'amende");
			}
			else if (amende>10 && amende<=15) {
				System.out.println("+ 1 pt");
			}
			else if (amende > 15) {
				System.out.println("bye le permis");
			}
			return true;
		}
		return false;
	}
}
