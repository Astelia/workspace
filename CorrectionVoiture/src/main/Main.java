package main;

import java.util.Random;
import java.util.Scanner;

import objets.Radar;
import objets.Voiture;

public class Main {

	public static void main(String[] args) {
		
		Voiture v1 = new Voiture("bleu", "1", 40, 180, true);
		Voiture v2 = new Voiture("blanc", "2", 30, 180, true);
		Voiture v3 = new Voiture("rouge", "3", 0, 180, true);
		
		Scanner in = new Scanner(System.in);
		System.out.print("Entrer vitesse limite : ");
		int vLimit = in.nextInt();
		in.nextLine();
		
		Radar radar = new Radar(vLimit);
		
		while (true) {
			v1.accelerer(aleatoireInferieur(16));
			if (radar.scan(v1)) {
				break;
			}
			v2.accelerer(aleatoireInferieur(16));
			if (radar.scan(v2)) {
				break;
			}
			v3.accelerer(aleatoireInferieur(16));
			if (radar.scan(v3)) {
				break;
			}
		}
		
		in.close();
	}

	
	public static int aleatoireInferieur(int max) {
		return new Random().nextInt(max);
	}
	
}
