package objets;

public class Client {

	private String id;
	private String nom;
	private String prenom;
	private String dateNaiss;
	private String email;
	private int nbComptes;
	
	private static int nbComptesMax;
	
	
	public Client(String ident, String n, String p, String date, String e) {
		setId(ident);
		setNom(n);
		setPrenom(p);
		setDateNaiss(date);
		setEmail(e);
		setNbComptes(0);
		setNbComptes(3);
	}


	public String getId() {
		return id;
	}


	public void setId(String ident) {
		if (verifId(ident)) {
			id = ident;
		}
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String n) {
		nom = n;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String p) {
		prenom = p;
	}


	public String getDateNaiss() {
		return dateNaiss;
	}


	public void setDateNaiss(String dateNaiss) {
		this.dateNaiss = dateNaiss;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public int getNbComptes() {
		return nbComptes;
	}


	public void setNbComptes(int nbComptes) {
		this.nbComptes = nbComptes;
	}


	public static int getNbComptesMax() {
		return nbComptesMax;
	}


	public static void setNbComptesMax(int nbComptesMax) {
		Client.nbComptesMax = nbComptesMax;
	}

	public static boolean verifId(String ident) {
		return Banque.testLongueur(8, ident) && Banque.testStringUpperCase(0, 1, ident) && Banque.testStringDigit(2, 7, ident);
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dateNaiss=" + dateNaiss + ", email="
				+ email + ", nbComptes=" + nbComptes + "]";
	}
	
	
}
