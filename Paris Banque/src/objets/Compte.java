package objets;

public class Compte {

	private String numeroCompte;
	private String codeAgence;
	private Client client;
	private float solde;
	private boolean decouvert;
	
	public Compte(String n, String code, Client client, boolean d) {
		setNumeroCompte(n);
		setCodeAgence(code);
		setClient(client);
		setSolde(0);
		setDecouvert(d);
	}

	
	public void retirer(float argent) {
		solde -= argent;
	}
	
	public void ajouter(float argent) {
		solde += argent;
	}
	
	public String getNumeroCompte() {
		return numeroCompte;
	}

	public void setNumeroCompte(String nC) {
		if (verifNumCompte(nC)) {
			numeroCompte = nC;
		}
	}

	public String getCodeAgence() {
		return codeAgence;
	}

	public void setCodeAgence(String cA) {
		if (Agence.verifCode(cA)) {
			codeAgence = cA;
		}
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client c) {
		client = c;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float s) {
		solde = s;
	}

	public boolean isDecouvert() {
		return decouvert;
	}

	public void setDecouvert(boolean d) {
		decouvert = d;
	}

	public static boolean verifNumCompte(String nC) {
		return Banque.testLongueur(11, nC) && Banque.testStringDigit(0, nC.length()-1, nC);
	}
	
	@Override
	public String toString() {
		return "Compte [numeroCompte=" + numeroCompte + ", codeAgence=" + codeAgence + ", client=" + client + ", solde="
				+ solde + ", decouvert=" + decouvert + "]";
	}
	
	
}
