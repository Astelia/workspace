package objets;

public class Agence {

	private String code;
	private String nom;
	private String prenom;
	
	private static int nbClientsMax;
	
	
	public Agence(String c, String n, String p) {
		setCode(c);
		setNom(n);
		setPrenom(p);
		setNbClientsMax(20);
	}

	
	public String getCode() {
		return code;
	}

	
	public void setCode(String c) {
		if (verifCode(c)) {
			code = c;
		}
	}
	
	
	public String getNom() {
		return nom;
	}

	public void setNom(String n) {
		nom = n;
	}


	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String p) {
		prenom = p;
	}


	public static int getNbClientsMax() {
		return nbClientsMax;
	}


	public static void setNbClientsMax(int nbCM) {
		nbClientsMax = nbCM;
	}

	
	public static boolean verifCode(String code) {
		return Banque.testLongueur(3, code) && Banque.testStringDigit(0, 2, code);
	}
	
	
	@Override
	public String toString() {
		return "Agence [code=" + code + ", nom=" + nom + ", prenom=" + prenom + "]";
	}
	
	
}
