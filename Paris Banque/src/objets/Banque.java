package objets;

import java.util.Scanner;

import objets.Client;
import objets.Compte;

public class Banque {
	
	private Agence[] agences;
	private Client[] clients;
	private Compte[] comptes;
	private int nbAgences;
	
	private static int nbAgencesMax;
	
	
	public Banque() {
		setNbAgencesMax(20);
		setClients(new Client[0]);
		setComptes(new Compte[0]);
		setNbAgences(0);
		setAgences(new Agence[nbAgencesMax]);
	}
	
	public void menu() {
		affichageMenu();
		System.out.print("Entrer le chiffre correspondant a votre besoin : ");
		char chiffre = scannerCharacter();
		System.out.println();
		switch (chiffre) {
			case '1' : {	int i = 0;
							while (i<nbAgencesMax && agences[i] != null) {
								i++;
							}
							creerAgence(i);
							break;
						}
			case '2' : {	int i = clients.length;
							nouveauTabClient(clients);
							creerClient(i);
							break;
						}
			case '3' : {	int i = comptes.length;
							nouveauTabCompte(comptes);
							creerCompte(i);
							break;
						}
			case '4' : {	System.out.print("Saisir numero ce compte : ");
							rechercheCompte(scannerString());
							break;
						}				
			case '5' : {	System.out.print("Saisir identifiant client : ");
							rechercheClient(scannerString());
							break;
						}
			case '6' : {	System.out.print("Saisir identifiant client : ");
							afficherListeComptesClient(scannerString());
							break;
						}
			case '7' : {	System.out.print("Saisir identifiant client : ");
							imprimerInfosClient(scannerString());
							break;
						}
		}
		quitter(chiffre);
	}
	
	private void quitter(char chiffre) {
		if (chiffre != '8') {
			menu();
		}
	}
	
	
	// Recherches des comptes d'un client a partir de son identifiant
	private void imprimerInfosClient(String id) {
		int i=0;
		while ( i<clients.length && !id.equals(clients[i].getId()) ) {
			i++;
		}
		if (i != clients.length) {
			Compte[] compt = listeComptesClient(id);
			affichageInfosClient(clients[i], compt);
		}
		else {
			System.out.println("Le client n'existe pas !");
		}
	}
	
	private Compte[] listeComptesClient(String id) {
		int i=0;
		int cpt=0;
		Compte[] compt = new Compte[3];
		while ( i<comptes.length && cpt<4 && !id.equals(comptes[i].getClient().getId()) ) {
			compt[cpt] = comptes[i];
			i++;
			cpt++;
		}
		return compt;
	}
	
	private void afficherListeComptesClient(String id) {
		Compte[] compt = listeComptesClient(id);
		if (compt.length != 0) {
			for (int i=0; i<compt.length; i++) {
				System.out.println(compt[i]);
			}
		}
		else {
			System.out.println("Le client n'a pas de compte(s) !");
		}
	}
	
	
	// Creation tableau client
	private void nouveauTabClient(Client[] clients) {
		Client[] res = new Client[clients.length + 1];
		for (int i=0; i<clients.length; i++) {
			res[i] = clients[i];
		}
		clients = res;
	}
		
	private void creerClient(int indice) {
		System.out.print("Saisir identifiant client : ");
		String id = scannerString();
		if (Client.verifId(id)) {
			System.out.print("Saisir nom : ");
			String nom = scannerString();
			System.out.print("Saisir prenom : ");
			String prenom = scannerString();
			System.out.print("Saisir date de naissance : ");
			String date = scannerString();
			System.out.print("Saisir email : ");
			String email = scannerString();
			clients[indice] = new Client(id, nom, prenom, date, email);
		}
		else {
			System.out.println("Creation de client impossible !");
			System.out.println("L'identifiant du client est invalide "
					+ "(2 caracteres majuscules + 6 chiffres attendus).");
		}
	}
		
	// Recherche dans le tableau de clients
	private void rechercheClient(String cl) {
		Client client = retourneClient(cl);
		if (client != null) {
			System.out.println(client);
		}
		else {
			System.out.println("Le client n'existe pas !");
		}
	}
	
	private Client retourneClient(String id) {
		int i=0;
		while ( i<clients.length && !id.equals(clients[i].getId()) ) {
			i++;
		}
		if (i != clients.length) {
			return clients[i];
		}
		else {
			return null;
		}
	}
	
	private boolean verifClientExiste(String id) {
		return retourneClient(id) != null;
	}
	

	// Creation tableau de comptes
	private void nouveauTabCompte(Compte[] comptes) {
		Compte[] res = new Compte[comptes.length + 1];
		for (int i=0; i<comptes.length; i++) {
			res[i] = comptes[i];
		}
		comptes = res;
	}
	
	private void creerCompte(int indice) {
		System.out.print("Saisir numero de compte : ");
		String nC = scannerString();
		System.out.print("Saisir code agence : ");
		String code = scannerString();
		System.out.print("Saisir identifiant client : ");
		String idClient = scannerString();
		if (Compte.verifNumCompte(nC) && verifAgenceeExiste(code) 
					&& verifClientExiste(idClient) && verifNbComptes(idClient)) {
			System.out.print("Possibilite de decouvert ? (oui/non) : ");
			boolean decouvert = scannerBool();
			Client client = retourneClient(idClient);
			client.setNbComptes(client.getNbComptes()+1);
			comptes[indice] = new Compte(nC, code, client, decouvert);
			
		}
		else {
			System.out.println("Creation de comptet impossible !");
			System.out.println("L'identifiant du client est invalide "
					+ "(2 caracteres majuscules + 6 chiffres attendus).");
		}
	}
	
	public boolean verifNbComptes(String id) {
		Client client = retourneClient(id);
		return client.getNbComptes() < Client.getNbComptesMax();
	}
	
	// Recherches dans le tableau de comptes
	private void rechercheCompte(String nC) {
		Compte compte = retourneCompte(nC);
		if (compte != null) {
			System.out.println(compte);
		}
		else {
			System.out.println("Le compte n'existe pas !");
		}
	}
	
	private Compte retourneCompte(String com) {
		int i=0;
		while ( i<comptes.length && !com.equals(comptes[i].getNumeroCompte()) ) {
			i++;
		}
		if (i != comptes.length) {
			return comptes[i];
		}
		else {
			return null;
		}
	}
	
	private boolean verifCompteExiste(String compte) {
		return retourneCompte(compte) != null;
	}
	
	
	// Recherches dans le tableau agence
	private void creerAgence(int indice) {
		System.out.print("Saisir code agence : ");
		String code = scannerString();
		if (Agence.verifCode(code)) {
			System.out.print("Saisir nom : ");
			String nom = scannerString();
			System.out.print("Saisir adresse : ");
			String adresse = scannerString();
			agences[indice] = new Agence(code, nom, adresse);
		}
		else {
			System.out.println("Creation d'agence impossible !");
			System.out.println("Le code agence est invalide (3 chiffres attendus).");
		}
	}
	
	private void rechercheAgence(String ag) {
		Agence agence = retourneAgence(ag);
		if (agence != null) {
			System.out.println(agence);
		}
		else {
			System.out.println("L'agence n'existe pas !");
		}
	}
	
	private Agence retourneAgence(String ag) {
		int i=0;
		while ( i<agences.length && !ag.equals(agences[i].getCode()) ) {
			i++;
		}
		if (i != agences.length) {
			return agences[i];
		}
		else {
			return null;
		}
	}
	
	private boolean verifAgenceeExiste(String agence) {
		return retourneAgence(agence) != null;
	}
	
	
	// Menus
	private void affichageMenu() {
		System.out.println();
		System.out.println("Menu :");
		System.out.println();
		System.out.println("1- Creer une agence");
		System.out.println("2- Creer un client");
		System.out.println("3- Creer un compte bancaire");
		System.out.println("4- Recherche de compte (numero de compte)");
		System.out.println("5- Recherche de client (Nom, Numero de compte, identifiant de client)");
		System.out.println("6- Afficher la liste des comptes d'un client (identifiant client)");
		System.out.println("7- Imprimer les infos client (identifiant client)");
		System.out.println("8- Quitter le programme");
	}
	
	private void affichageInfosClient(Client client, Compte[] cb) {
		System.out.println("                            Fiche client                             ");
		System.out.println();
		System.out.println("Numero client : "+client.getId());
		System.out.println("Nom : "+client.getNom());
		System.out.println("Prenom : "+client.getPrenom());
		System.out.println("Date de naissance : "+client.getDateNaiss());
		System.out.println();
		System.out.println("_____________________________________________________________________");
		System.out.println("Liste de compte");
		System.out.println("_____________________________________________________________________");
		System.out.println("Numero de compte                      Solde");
		for (int i=0; i<cb.length; i++) {
			System.out.println(cb[i].getNumeroCompte()+"                       "
								+cb[i].getSolde()+"               "+smiley(cb[i].getSolde()));
		}
	}
	
	private String smiley(float solde) {
		if (solde < 0) {
			return ":-(";
		}
		else {
			return ":-)";
		}
	}
	
	public Agence[] getAgences() {
		return agences;
	}

	public void setAgences(Agence[] a) {
		agences = a;
	}

	public Client[] getClients() {
		return clients;
	}

	public void setClients(Client[] c) {
		clients = c;
	}

	public Compte[] getComptes() {
		return comptes;
	}

	public void setComptes(Compte[] c) {
		comptes = c;
	}

	public int getNbAgences() {
		return nbAgences;
	}

	public void setNbAgences(int nbA) {
		nbAgences = nbA;
	}

	public static int getNbAgencesMax() {
		return nbAgencesMax;
	}

	public static void setNbAgencesMax(int nbAM) {
		nbAgencesMax = nbAM;
	}
	
	public static char scannerCharacter() {
		Scanner scanner = new Scanner(System.in);
		String mot = scanner.next();
		scanner.nextLine();
		return mot.charAt(0);
	}
	
	public static boolean scannerBool() {
		System.out.println("Le decouvert est-il autorise ? (oui/non) : ");
		Scanner scanner = new Scanner(System.in);
		String mot = scanner.next();
		scanner.nextLine();
		if (mot.equals("oui")) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public static String scannerString() {
		Scanner scanner = new Scanner(System.in);
		String mot = scanner.next();
		scanner.nextLine();
		return mot;
	}

	public static boolean testLongueur(int l, String str) {
		return str.length() == l;
	}
	
	public static boolean testStringDigit(int debut, int fin, String str) {
		int i = debut;
		while (i<=fin && Character.isDigit(str.charAt(i))) {
			i++;
		}
		return i == str.length();
	}
	
	public static boolean testStringUpperCase(int debut, int fin, String str) {
		int i = debut;
		while (i<=fin && Character.isUpperCase(str.charAt(i))) {
			i++;
		}
		return i == str.length();
	}
	
}
